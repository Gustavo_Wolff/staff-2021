package test;

import static org.junit.Assert.*;

import main.AnaoBarbaLonga;
import main.DadoFalso;

import org.junit.Test;

public class AnaoBarbaLongaTest {
    @Test
    public void anaoDevePerderVida66PorCento() {
        DadoFalso dado = new DadoFalso();
        dado.simularValor(4);
        AnaoBarbaLonga balin = new AnaoBarbaLonga( "Balin", dado );
        balin.sofrerDano();
        assertEquals( 100.0, balin.getVida(), 1e-8 );
    }

    @Test
    public void anaoNaoDevePerderVida33PorCento() {
        DadoFalso dado = new DadoFalso();
        dado.simularValor(5);
        AnaoBarbaLonga balin = new AnaoBarbaLonga( "Balin", dado );
        balin.sofrerDano();
        assertEquals( 110.0, balin.getVida(), 1e-8 );
    }
}