package test;

import static org.junit.Assert.*;

import main.DataTerceiraEra;

import org.junit.Test;

public class DataTerceiraEraTest {

    @Test
    public void isFimDaGuerraDoAnelBissexto() {
        DataTerceiraEra fimDaGuerraDoAnel = new DataTerceiraEra(1, 10, 3019);

        System.out.println(fimDaGuerraDoAnel.ehBissexto());

        assertFalse(fimDaGuerraDoAnel.ehBissexto());
    }

    @Test
    public void is2020Bissexto() {
        DataTerceiraEra futuro = new DataTerceiraEra(15, 6, 2024);
        System.out.println(futuro.ehBissexto());
        assertTrue(futuro.ehBissexto());
    }
}
