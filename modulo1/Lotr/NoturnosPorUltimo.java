import java.util.*;

public class NoturnosPorUltimo implements Atacar {
    private static ArrayList<Status> STATUS_VIVOS = new ArrayList<> (
        Arrays.asList(
            Status.RECEM_CRIADO,
            Status.SOFREU_DANO
        )
    );
    
    public ArrayList<Elfo> atacar ( ArrayList<Elfo> atacantes ) {
        ArrayList<Elfo> ordemDeAtaque = new ArrayList<>();
        for (Elfo elfo : atacantes ) {
            if (estaVivo(elfo) ) {
                if ( elfo.getClass() == ElfoVerde.class ) {
                    ordemDeAtaque.add(elfo);
                }
            }
        }
        for (Elfo elfo : atacantes ) {
            if (estaVivo(elfo)) {
                if ( elfo.getClass() == ElfoNoturno.class ) {
                    ordemDeAtaque.add(elfo);
                }
            }
        }
        return ordemDeAtaque;
    }
    
    private boolean estaVivo ( Elfo elfo ) {
        return STATUS_VIVOS.contains( elfo.getStatus() );
    }
}
