import java.util.*;

public class Exercito {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<> (
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    private ArrayList<Elfo> exercito = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    private HashMap<Class, ArrayList<Elfo>> porClasse = new HashMap<>();
    public Atacar atacar;
    
    public Exercito () {
        this.atacar = new NoturnosPorUltimo();
    }
    
    public Exercito( Atacar atacar ) {
        this.atacar = atacar;
    }
    
    public ArrayList<Elfo> getList () {
        return this.exercito;
    }
    
    public ArrayList<Elfo> buscarPorStatus ( Status status ) {
        return this.porStatus.get( status );
    }
    
    public ArrayList<Elfo> buscarPorClasse ( Class classe ) {
        return this.porClasse.get( classe );
    }
    
    public Elfo getElfo ( int posicao )  {
        return this.exercito.get(posicao);
    }
    
    public void recrutar ( Elfo elfo ) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains( elfo.getClass() );
        
        if( podeAlistar ) {
            exercito.add( elfo );
            
            ArrayList<Elfo> elfoDeUmStatus = porStatus.get( elfo.getStatus() );
            if( elfoDeUmStatus == null ) {
                elfoDeUmStatus = new ArrayList<>();
                porStatus.put( elfo.getStatus(), elfoDeUmStatus );
            }
            elfoDeUmStatus.add( elfo );
            
            ArrayList<Elfo> elfoDeUmaClasse = porClasse.get( Elfo.class );
            if( elfoDeUmaClasse == null ){
                elfoDeUmaClasse = new ArrayList<>();
                porClasse.put( Elfo.class, elfoDeUmaClasse );
            }
            elfoDeUmaClasse.add( elfo );
        }
    }
    
    public void realizarAtaque ( ArrayList<Elfo> atacantes, Anao anao ) {
        for (Elfo elfo : atacantes ) {
            elfo.atirarFlecha(anao);
        }
    }
}
