db.banco.drop();
db.agencias.drop();
db.banco.drop();
db.endereco.drop();
db.pais.drop();
db.estado.drop();
db.cidade.drop();
db.cliente.drop();
db.gerente.drop();
db.conta.drop();
db.movimentacoes.drop();


//db.printCollectionStats()

db.createCollection("banco");
db.banco.insertMany({alfa,beta,omega});

alfa = 
{
        nome:"alfa",
        codigo: "011",
        agencias:[alfaWeb, alfaCalifornia, alfaLondres]
};

beta = 
{
        nome:"beta",
        codigo: "241",
        agencias:[betaWeb]
};

omega = 
{
        nome:"omega",
        codigo: "307",
        agencias:[omegaWeb, omegaItu, omegaHermana]
};
//=========================== collection agencias =====================

db.createCollection("agencias");
db.agencias.insertMany({alfaWeb, alfaCalifornia, alfaLondres, betaWeb, omegaWeb, omegaItu, omegaHermana});

//=========================== agencias alfa =====================
 
 alfaWeb = 
 {
     codigoAgencia:"0001",
     nome:"web",
     endereco: enderecoAlfaWeb,
     conta:[conta01, conta02, conta03],
     consolidacao:[consolidacao01]
 };
 
  alfaCalifornia = 
 {
     codigoAgencia:"0002",
     nome:"california",
     endereco: enderecoAlfaCalifornia,
     conta:[conta01],
     consolidacao:[consolidacao01, consolidacao02]
 };
 
   alfaLondres = 
 {
     codigoAgencia:"0101",
     nome:"londres",
     endereco: enderecoAlfaLondres,
     conta:[conta03],
     consolidacao:[consolidacao02]
 };
 
//=========================== agencias beta =====================
 
  betaWeb = 
 {
     codigoAgencia:"0001",
     nome:"web",
     endereco: enderecoBetaWeb,
     conta:[conta02],
     consolidacao:[consolidacao01]
 };

//=========================== agencias omega ===================== 
 
  omegaWeb = 
 {
     codigoAgencia:"0001",
     nome:"web",
     endereco: enderecoOmegaWeb,
     conta:[conta03, conta02],
     consolidacao:[consolidacao02]
 };
 
   omegaItu = 
 {
     codigoAgencia: "8761",
     nome:"itu",
     endereco: enderecoOmegaItu,
     conta:[conta02],
     consolidacao:[consolidacao01]
 }; 
 
   omegaHermana = 
 {
     codigoAgencia:"4567",
     nome:"hermana",
     endereco:enderecoOmegaHermana,
     conta:[conta01, conta03],
     consolidacao:[consolidacao01, consolidacao02]
 };

//=========================== collection consolidacoes  =====================

db.createCollection("consolidacoes");
db.consolidacoes.insertMany({consolidacao01, consolidacao02});



//=========================== consolidacoes  ===================== 
 
consolidacao01 =
{
    saldo: 10000,
    saques: 2500,
    depositos: 5300,
    numero_correntistas: 2
    
}

consolidacao02 =
{
    saldo: 0,
    saques: 0,
    depositos: 0,
    numero_correntistas: 0
    
}

//=========================== collection endereco  =====================

db.createCollection("endereco");
db.endereco.insertMany({enderecoAlfaWeb, enderecoAlfaCalifornia, enderecoAlfaLondres, enderecoBetaWeb, enderecoOmegaWeb, enderecoOmegaItu, enderecoOmegaHermana, endereco01, endereco02});

//=========================== enderecos agencias alfa ===================== 

enderecoAlfaWeb = 
{
    logradouro: "Rua Testando",
    numero: "55",
    complemento: "loja 1",
    cep: "NA",
    bairro: bairroNA
};

enderecoAlfaCalifornia = 
{
    logradouro: "Rua Testing",
    numero: "122",
    complemento: "NA",
    cep: "NA",
    bairro: bairroBHPS
};

enderecoAlfaLondres = 
{
    logradouro: "Tesing",
    numero: "525",
    complemento: "NA",
    cep: "NA",
    bairro: bairroCroydon 
};

//=========================== enderecos agencias beta ===================== 

enderecoBetaWeb = 
{
    logradouro: "Rua Testando",
    numero: "55",
    complemento: "loja 2",
    cep: "NA",
    bairro: bairroNA
};

//=========================== enderecos agencias omega ===================== 

enderecoOmegaWeb = 
{
    logradouro: "Rua Testando",
    numero: "55",
    complemento: "loja 3",
    cep: "NA",
    bairro: bairroNA
};

enderecoOmegaItu = 
{
    logradouro: "Rua do meio",
    numero: "2233",
    complemento: "NA",
    cep: "NA",
    bairro: bairroQualquer
};

enderecoOmegaHermana = 
{
    logradouro: "Rua do boca",
    numero: "222",
    complemento: "NA",
    cep: "NA",
    bairro: bairroCaminito
  
};


//=========================== collection pais ===================== 

db.createCollection("pais");
db.pais.insertMany({brasil, argentina, inglaterra, eua});

brasil = 
{
    nome: "brasil"
};

argentina = 
{
    nome:"argentina"
};

inglaterra =
{
    nome:"inglaterra"
};

eua = 
{
    nome: "Estados Unidos"
};

//=========================== collection estado ===================== 

db.createCollection("estado");
db.estado.insertMany({ estadoNA, estadoSP,estadoCaminito, estadoBoroughs, estadoCalifornia} );

estadoNA = 
{
    pais: brasil,
    estado: "NA"
};

estadoSP = 
{
    pais: brasil,
    estado: "sao paulo"
};

estadoBuenosAires = 
{
    pais: argentina,
    estado: "buenos aires"
    
};

estadoBoroughs =
{
    pais: inglaterra,
    estado:"croydon"
};

estadoCalifornia = 
{
    pais: eua,
    estado: "california"
};

//=========================== collection cidade ===================== 
db.createCollection("cidade");
db.cidade.insertMany({ cidadeNa, cidadeSanFrancisco, cidadeLondres, cidadeItu, cidadeBuenosAires} );

cidadeNa = 
{
    cidade: "NA",
    estado: estadoNA,
}

cidadeSanFrancisco = 
{
    cidade: "San Franscisco",
    estado: estadoCalifornia
}

cidadeLondres = 
{
    cidade: "Londres",
    estado: estadoBoroughs
}

cidadeItu = 
{
    cidade: "Itu",
    estado: estadoSP
}

//=========================== collection bairro =====================
db.createCollection("bairro");
db.bairro.insertMany( {bairroNA, bairroBHPS, bairroCroydon, bairroQualquer, bairroCaminito});

bairroNA = 
{
    bairro: "NA",
    cidade: cidadeNa
}

bairroBHPS = 
{
    bairro: "Between Hydeand Powell Streets",
    cidade: cidadeSanFrancisco
}

bairroCroydon = 
{
    bairro: "Croydon",
    cidade: cidadeLondres
}

bairroQualquer = 
{
    bairro: "Qualquer",
    cidade: cidadeItu
}

bairroCaminito = 
{
    bairro: "Caminito",
    cidade: cidadeBuenosAires
}

//=========================== collection cliente =====================

db.createCollection("cliente");
db.cliente.insertMany({cliente01, cliente02});

cliente01 = 
{
    nome: "Juanito",
    cpf: "770.000.000-42",
    estado_civil: "ferrado",
    data_nascimento: "06/06/1969",
    endereco: endereco01,
}

cliente02 = 
{
    nome: "Fuentes",
    cpf: "770.000.000-43",
    estado_civil: "+ferrado",
    data_nascimento: "07/06/1989",
    endereco: endereco02,
}

//=========================== collection gerente =====================

db.createCollection("gerente");
db.gerente.insertMany({gerente01, gerente02});

gerente01 = 
{
    nome: "El Capitan",
    cpf: "870.700.000-42",
    estado_civil: "suavao",
    data_nascimento: "06/06/1977",
    endereco: endereco01,
}

gerente02 = 
{
    nome: "EL Kabong",
    cpf: "789.000.000-43",
    estado_civil: "suave",
    data_nascimento: "07/11/1989",
    endereco: endereco02,
}

//=========================== collection conta =====================

db.createCollection("conta");
db.conta.insertMany({conta01, conta02, conta03});

conta01 = 
{
    codigo: 0001,
    saldo: 0,
    tipo_conta: "CONJUNTA",
    movimentacoes: [credito, debito],
    clientes: [ cliente01, cliente02 ],
    gerentes: [gerente02] 
}

conta02 = 
{
    codigo: 0002,
    saldo: 2500,
    tipo_conta: "PJ",
    movimentacoes: [credito],
    clientes: [ cliente01],
    gerentes: [gerente02, gerente01] 
}

conta03 = 
{
    codigo: 0003,
    saldo: 5000,
    tipo_conta: "PF",
    movimentacoes: [debito],
    clientes: [cliente02],
    gerentes: [gerente01] 
}

//=========================== collection conta =====================

db.createCollection("movimentacoes");
db.movimentacoes.insertMany({debito, credito});

debito =
{
    debito: 100
}

credito =
{
    credito: 200
}


db.unknown.find({})

