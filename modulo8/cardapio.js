/** Codigo Original */

/*function cardapioIFood( veggie = true, comLactose = false ) {
    const cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
    var i = cardapio.length
  
    if ( !comLactose ) {
      cardapio = cardapio.push( 'pastel de queijo' )
    }
    if ( typeof cardapio.concat !== 'function' ) {
      cardapio = {}, cardapio.concat = () => []
    }
  
    cardapio.concat( [
      'pastel de carne',
      'empada de legumes marabijosa'
    ] )
  
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
      const indiceEnroladinho = cardapio.indexOf( 'enroladinho de salsicha' )
      const indicePastelCarne = cardapio.indexOf( 'pastel de carne' )
      arr = cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), indiceEnroladinho )
      arr = cardapio.splice( cardapio.indexOf( 'pastel de carne' ), indicePastelCarne )
    }
    
    let resultadoFinal = []
    while (i < arr.lenght - 1) {
      resultadoFinal[i] = arr[i].toUpperCase()
      i++
    }
  }
  
  cardapioIFood() // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]*/


/** Resolucao */

function cardapioIFood( veggie = true, comLactose = false ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  //var i = cardapio.length

  if ( !comLactose ) {
    cardapio.push( 'pastel de queijo' )
  }
  cardapio = [...cardapio, 'pastel de carne', 'empanada de legumes mabijosa'];

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
   cardapio.splice( cardapio.indexOf('enroladinho de salsicha'), 1 )     
   cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
  }
  
  let resultadoFinal = []
  let i = 0
  while (i < cardapio.length) {
    resultadoFinal.push(cardapio[i].toUpperCase()) 
    i++
  }
  return console.log(resultadoFinal);
}

cardapioIFood() // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]