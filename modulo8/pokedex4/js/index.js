const pokeApi = new PokeApi();

let idNaTela = 0;

const renderizar = ( pokemon ) => {
  const dadosPokemon = document.getElementById( 'pokedex' );
  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = `Nome: ${ pokemon.nome }`;

  const pokeId = dadosPokemon.querySelector( '.pokeId' );
  pokeId.innerHTML = `Id: ${ pokemon.pokeId }`;
  idNaTela = pokemon.pokeId;

  const imgPokemon = dadosPokemon.querySelector( '.thumb' );
  imgPokemon.src = pokemon.imagem;
  imgPokemon.alt = `Imagem do pokemon ${ pokemon.nome }`

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Peso: ${ pokemon.peso }`;

  // Tipos
  const tipos = dadosPokemon.querySelector( '.tipo' );
  tipos.innerHTML = 'Tipo(s):'
  const ul = document.createElement( 'ul' );
  tipos.appendChild( ul );
  // eslint-disable-next-line array-callback-return
  pokemon.tipos.map( tipo => {
    const li = document.createElement( 'li' );
    li.innerHTML = `${ tipo.type.name } `;
    ul.appendChild( li );
  } );

  // Estatistica
  const estatistica = dadosPokemon.querySelector( '.estatistica' );
  estatistica.innerHTML = 'Estatísticas:';
  const ulStat = document.createElement( 'ul' );
  estatistica.appendChild( ulStat );
  // eslint-disable-next-line array-callback-return
  pokemon.estatistica.map( stats => {
    const li = document.createElement( 'li' );
    li.innerHTML = `
          Nome: ${ stats.stat.name }
          * Valor base:  ${ stats.base_stat }%
          * Esforço: ${ stats.effort }%
          `;
    ulStat.appendChild( li );
  } );
}

// eslint-disable-next-line no-unused-vars
const buscarPorId = () => {
  const idPokemon = document.getElementById( 'idPokemon' ).value;
  let paraErro = document.getElementById( 'paraErro' );
  // eslint-disable-next-line eqeqeq
  if ( idNaTela == idPokemon ) {
    return;
  }
  pokeApi
    .buscarEspecifico( idPokemon )
    .then( pokemon => {
      paraErro.innerHTML = '';
      const poke = new Pokemon( pokemon )
      renderizar( poke );
    }, err => {
      paraErro = document.getElementById( 'paraErro' );
      paraErro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}

// eslint-disable-next-line no-unused-vars
const buscarOnChange = () => {
  const paraErro = document.getElementById( 'paraErro' );
  const idPokemon = document.getElementById( 'idPokemon' ).value;
  pokeApi
    .buscarEspecifico( idPokemon )
    .then( () => {
      paraErro.innerHTML = '';
    }, err => {
      paraErro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}

// eslint-disable-next-line no-unused-vars
const buscarComSorte = () => {
  let paraErro = document.getElementById( 'paraErro' );
  const randomNumber = ( min, max ) => Math.floor( Math.random() * ( max - min + 1 ) ) + min
  const idBusca = randomNumber( 1, 893 );
  // eslint-disable-next-line eqeqeq
  const numJafoi = window.localStorage.getItem( idBusca );
  // eslint-disable-next-line eqeqeq
  if ( numJafoi == idBusca ) {
    return;
  }
  const idPokemon = document.getElementById( 'idPokemon' );
  idPokemon.value = idBusca;
  window.localStorage.setItem( idBusca, idBusca );

  pokeApi
    .buscarEspecifico( idBusca )
    .then( pokemon => {
      paraErro.innerHTML = '';
      const poke = new Pokemon( pokemon )
      renderizar( poke );
    }, err => {
      paraErro = document.getElementById( 'paraErro' );
      paraErro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}

// eslint-disable-next-line no-unused-vars
const limparStorage = () => window.localStorage.clear();
