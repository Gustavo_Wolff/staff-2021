package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.net.URISyntaxException;

@SpringBootTest
@AutoConfigureMockMvc
public class TipoContatoControllerTest  {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void buscarTodos() throws Exception {
        URI uri = new URI("/cw/tipoContato/");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }

    @Test
    public void salvarTipoContato() throws Exception {
        URI uri = new URI("/cw/tipoContato/salvar");

        TipoContatoEntity tipo = new TipoContatoEntity();
        tipo.setNome("telefone");

        mockMvc
                .perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(tipo)))
                .andExpect(MockMvcResultMatchers.status().is(403));

    }
}
