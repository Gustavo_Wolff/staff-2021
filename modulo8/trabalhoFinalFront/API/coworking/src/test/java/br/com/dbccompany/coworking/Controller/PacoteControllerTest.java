package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.PacoteEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class PacoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void buscarPacotes() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/cw/pacote/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }

    @Test
    public void salvarPacote() throws Exception {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(500.0);

        mockMvc
                .perform(MockMvcRequestBuilders.post("/cw/pacote/salvar/").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(pacote)))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }
}
