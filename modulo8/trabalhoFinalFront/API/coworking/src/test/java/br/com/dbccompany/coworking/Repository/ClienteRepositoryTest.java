package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import jdk.vm.ci.meta.Local;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void criarCliente(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("email");
        tp2.setNome("telefone");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont1 = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont1.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont1.setContato("paula@gmail.com");
        cont2.setContato("5551996543451");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont1);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        Character[] cpf = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1'};
        cliente.setCpf(cpf);
        repository.save(cliente);
        assertEquals(cliente.getNome(), repository.findByNome(cliente.getNome()).get().getNome());
    }
}
