package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class Espaco_X_PacoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void buscarEspacoPacotes() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/cw/espacoPacote/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }
}
