package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Exception.Espaco.ErroAoRegistrarEspaco;
import br.com.dbccompany.coworking.Exception.Espaco.NenhumEspacoEncontrado;
import br.com.dbccompany.coworking.Service.EspacoService;
import br.com.dbccompany.coworking.Util.ConversorDinheiro;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/espaco")
public class EspacoController {

    @Autowired
    private EspacoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<EspacoDTO> salvar(@RequestBody EspacoDTO espaco){
        try{
            return new ResponseEntity<>(service.salvar(espaco.converter()), HttpStatus.ACCEPTED);
        }catch (ErroAoRegistrarEspaco e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> editar (@RequestBody EspacoDTO espaco, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(espaco.converter(), id), HttpStatus.ACCEPTED);
        }catch (ErroAoRegistrarEspaco e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<EspacoDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch (NenhumEspacoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        }catch (NenhumEspacoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public ResponseEntity<EspacoDTO> buscarNome(@PathVariable String nome){
        try{
            return new ResponseEntity<>(service.buscarPorNome(nome), HttpStatus.OK);
        }catch (NenhumEspacoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
