package br.com.dbccompany.coworking.Exception.Acesso;

public class AcessoInexistente extends AcessoException {

    public AcessoInexistente(){
        super("O acesso buscado não existe!");
    }
}
