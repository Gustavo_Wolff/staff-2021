package br.com.dbccompany.coworking.Exception.SaldoCliente;

public class PagamentoNaoFinalizado extends SaldoClienteException {
    public PagamentoNaoFinalizado(){
        super("Não foi possível finalizar o pagamento!");
    }
}
