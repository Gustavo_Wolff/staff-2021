package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.Cliente_X_PacoteDTO;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Cliente_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.ClientePacote.ErroAoRelacionarClienteEPacote;
import br.com.dbccompany.coworking.Exception.ClientePacote.NenhumRelacionamentoClientePacoteEncontrado;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.Cliente_X_PacoteRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Cliente_X_PacoteService {

    @Autowired
    private Cliente_X_PacoteRepository repository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    private List<Cliente_X_PacoteDTO> converterEntitiesParaDTO(List<Cliente_X_PacoteEntity> entities){
        List<Cliente_X_PacoteDTO> dtos = new ArrayList<>();
        for(Cliente_X_PacoteEntity elem : entities){
            dtos.add(new Cliente_X_PacoteDTO(elem));
        }
        return dtos;
    }

    @Transactional(rollbackFor = Exception.class)
    public Cliente_X_PacoteDTO salvar (Cliente_X_PacoteDTO clientePacote) throws ErroAoRelacionarClienteEPacote {
        try{
            clientePacote.setCliente(new ClienteDTO(clienteRepository.findById(clientePacote.getCliente().getId()).get()));
            clientePacote.setPacote(new PacoteDTO(pacoteRepository.findById(clientePacote.getPacote().getId()).get()));
            return new Cliente_X_PacoteDTO(repository.save(clientePacote.converter()));
        } catch (Exception e){
            throw new ErroAoRelacionarClienteEPacote();
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public Cliente_X_PacoteDTO editar (Cliente_X_PacoteDTO clientePacote, Integer id) throws ErroAoRelacionarClienteEPacote{
        try{
            clientePacote.setId(id);
            return salvar(clientePacote);
        } catch (Exception e){
            throw new ErroAoRelacionarClienteEPacote();
        }
    }

    public List<Cliente_X_PacoteDTO> buscarTodos() throws NenhumRelacionamentoClientePacoteEncontrado {
        try{
            return converterEntitiesParaDTO(repository.findAll());
        } catch (Exception e){
            throw new NenhumRelacionamentoClientePacoteEncontrado();
        }
    }

    public Cliente_X_PacoteDTO buscarPorId(Integer id) throws NenhumRelacionamentoClientePacoteEncontrado {
        Optional<Cliente_X_PacoteEntity> entity = repository.findById(id);
        if(entity.isPresent()){
            return new Cliente_X_PacoteDTO(entity.get());
        }
        throw new NenhumRelacionamentoClientePacoteEncontrado();
    }
}
