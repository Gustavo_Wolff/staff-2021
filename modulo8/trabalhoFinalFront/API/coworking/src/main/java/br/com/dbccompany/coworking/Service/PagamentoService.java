package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.*;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.Pagamento.PagamentoNaoEncontrado;
import br.com.dbccompany.coworking.Exception.SaldoCliente.PagamentoNaoFinalizado;
import br.com.dbccompany.coworking.Repository.Cliente_X_PacoteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository repository;

    @Autowired
    private SaldoClienteService saldoClienteService;

    @Autowired
    private Cliente_X_PacoteRepository cliente_x_pacoteRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;


    @Transactional(rollbackFor = Exception.class)
    public PagamentoDTO pagar (PagamentoDTO pagamentoDto) throws PagamentoNaoFinalizado {
        pagamentoDto.setContratacao(
                pagamentoDto.getContratacao() != null
                ? new ContratacaoDTO(contratacaoRepository.findById(pagamentoDto.getContratacao().getId()).get())
                : null
        );
        pagamentoDto.setClientePacote(
                pagamentoDto.getClientePacote() != null
                ? new Cliente_X_PacoteDTO(cliente_x_pacoteRepository.findById(pagamentoDto.getClientePacote().getId()).get())
                : null
        );

        PagamentoEntity pagamento = pagamentoDto.converter();
        if((pagamento.getContratacao() == null && pagamento.getClientePacote() == null)
           || (pagamento.getContratacao() != null && pagamento.getClientePacote() != null)){
            throw new PagamentoNaoFinalizado();
        }
        List<SaldoClienteDTO> saldosCliente = new ArrayList<>();

        if(pagamento.getContratacao() != null){
            PagamentoDTO pagamentoFinal = new PagamentoDTO(repository.save(pagamento));
            SaldoClienteEntity saldoGerado = saldoClienteService.pagarContratacao(pagamento.getContratacao());
            saldosCliente.add(new SaldoClienteDTO(saldoGerado));
            pagamentoFinal.setSaldosGerado(saldosCliente);
            return pagamentoFinal;
        }
        else{
            PagamentoDTO pagamentoFinal = new PagamentoDTO(repository.save(pagamento));
            List<SaldoClienteEntity> saldosGerados = saldoClienteService.pagarClientePacote(pagamento.getClientePacote());
            for(SaldoClienteEntity saldo : saldosGerados){
                saldosCliente.add(new SaldoClienteDTO(saldo));
            }
            pagamentoFinal.setSaldosGerado(saldosCliente);
            return pagamentoFinal;
        }
    }

    public List<PagamentoDTO> buscarTodos() throws PagamentoNaoEncontrado {
        try{
            return converterToDTO(repository.findAll());
        }catch (Exception e){
            throw new PagamentoNaoEncontrado();
        }
    }

    public PagamentoDTO buscarPorId(Integer id) throws PagamentoNaoEncontrado {
        Optional<PagamentoEntity> pagamento = repository.findById(id);
        if (pagamento.isPresent()){
            return new PagamentoDTO(pagamento.get());
        }
        throw new PagamentoNaoEncontrado();
    }

    public List<PagamentoDTO> converterToDTO(List<PagamentoEntity> pagamentos){
        List<PagamentoDTO> listaConvertida = new ArrayList<>();
        for(PagamentoEntity pagamento : pagamentos){
            listaConvertida.add(new PagamentoDTO(pagamento));
        }
        return listaConvertida;
    }
}
