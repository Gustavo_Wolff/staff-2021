package br.com.dbccompany.coworking.Entity;

public enum TipoContratacaoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES, ESPECIAL
}