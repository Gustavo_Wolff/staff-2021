package br.com.dbccompany.coworking.Exception.ClientePacote;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class ClientePacoteException extends GeneralException {


    public ClientePacoteException(String mensagem){
        super(mensagem);
    }
}
