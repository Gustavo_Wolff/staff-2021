package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.Espaco_X_PacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Exception.EspacoPacote.ErroAoIncluirEspacoEmPacote;
import br.com.dbccompany.coworking.Exception.EspacoPacote.NenhumEspacoPacoteEncontrado;
import br.com.dbccompany.coworking.Repository.Espaco_X_PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Espaco_X_PacoteService {

    @Autowired
    private Espaco_X_PacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Espaco_X_PacoteDTO salvar (Espaco_X_PacoteEntity espacoPacote) throws ErroAoIncluirEspacoEmPacote {
        try{
            return new Espaco_X_PacoteDTO(repository.save(espacoPacote));
        }catch (Exception e){
            throw new ErroAoIncluirEspacoEmPacote();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Espaco_X_PacoteDTO editar (Espaco_X_PacoteEntity espacoPacote, Integer id) throws ErroAoIncluirEspacoEmPacote {
        try{
            espacoPacote.setId(id);
            return salvar(espacoPacote);
        }catch (Exception e){
            throw new ErroAoIncluirEspacoEmPacote();
        }
    }

    private List<Espaco_X_PacoteDTO> conversorListaEntityParaDTO(List<Espaco_X_PacoteEntity> entities){
        List<Espaco_X_PacoteDTO> dtos = new ArrayList<>();
        for(Espaco_X_PacoteEntity elem : entities){
            dtos.add(new Espaco_X_PacoteDTO(elem));
        }
        return dtos;
    }

    public List<Espaco_X_PacoteDTO> buscarTodos() throws NenhumEspacoPacoteEncontrado {
        try{
            return conversorListaEntityParaDTO(repository.findAll());
        }catch (Exception e){
            throw new NenhumEspacoPacoteEncontrado();
        }

    }

    public Espaco_X_PacoteDTO buscarPorId(Integer id) throws NenhumEspacoPacoteEncontrado {
        Optional<Espaco_X_PacoteEntity> result = repository.findById(id);
        if(result.isPresent()){
            return new Espaco_X_PacoteDTO(result.get());
        }
        throw new NenhumEspacoPacoteEncontrado();
    }
}
