package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Exception.Pacote.ErroAoRegistrarPacote;
import br.com.dbccompany.coworking.Exception.Pacote.PacoteNaoEncontrado;
import br.com.dbccompany.coworking.Service.PacoteService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<PacoteDTO> salvar (@RequestBody PacoteDTO pacote){
       try{
           return new ResponseEntity<>(service.salvar(pacote.converter()), HttpStatus.CREATED);

       }catch(ErroAoRegistrarPacote e){
           LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
       }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<PacoteDTO> editar (@RequestBody PacoteDTO pacote, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(pacote.converter(), id), HttpStatus.CREATED);
        } catch( ErroAoRegistrarPacote e ){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<PacoteDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        } catch(PacoteNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<PacoteDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch(PacoteNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
