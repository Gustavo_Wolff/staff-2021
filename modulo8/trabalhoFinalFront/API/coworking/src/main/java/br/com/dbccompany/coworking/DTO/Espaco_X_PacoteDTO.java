package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

import java.util.List;

public class Espaco_X_PacoteDTO {

    private Integer id;
    private EspacoDTO espaco;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    public Espaco_X_PacoteDTO(Espaco_X_PacoteEntity espacoPacote){
        this.id = espacoPacote.getId();
        this.espaco = new EspacoDTO(espacoPacote.getEspaco());
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
    }

    public Espaco_X_PacoteDTO(){

    }

    public Espaco_X_PacoteEntity converter(){
        Espaco_X_PacoteEntity espacoPacote = new Espaco_X_PacoteEntity();
        espacoPacote.setId(this.id);
        espacoPacote.setEspaco(this.espaco.converter());
        espacoPacote.setTipoContratacao(this.tipoContratacao);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        return espacoPacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
