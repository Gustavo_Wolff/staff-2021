package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.Cliente_X_PacoteDTO;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.Exception.ClientePacote.ErroAoRelacionarClienteEPacote;
import br.com.dbccompany.coworking.Exception.ClientePacote.NenhumRelacionamentoClientePacoteEncontrado;
import br.com.dbccompany.coworking.Service.Cliente_X_PacoteService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/clientePacote")
public class Cliente_X_PacoteController {

    @Autowired
    private Cliente_X_PacoteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Cliente_X_PacoteDTO> salvar (@RequestBody Cliente_X_PacoteDTO clientePacote){
        try{
            return new ResponseEntity<>(service.salvar(clientePacote), HttpStatus.CREATED);
        } catch(ErroAoRelacionarClienteEPacote e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Cliente_X_PacoteDTO> editar (@RequestBody Cliente_X_PacoteDTO clientePacote, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(clientePacote, id), HttpStatus.CREATED);
        }catch (ErroAoRelacionarClienteEPacote e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<Cliente_X_PacoteDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch(NenhumRelacionamentoClientePacoteEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Cliente_X_PacoteDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch(NenhumRelacionamentoClientePacoteEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
