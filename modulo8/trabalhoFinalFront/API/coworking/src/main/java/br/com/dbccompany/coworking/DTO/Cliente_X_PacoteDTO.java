package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Cliente_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;

import java.util.ArrayList;
import java.util.List;

public class Cliente_X_PacoteDTO {

    private Integer id;
    private ClienteDTO cliente;
    private PacoteDTO pacote;
    private Integer quantidade;

    public Cliente_X_PacoteDTO(Cliente_X_PacoteEntity clientePacote){
        this.id = clientePacote.getId();
        this.cliente = new ClienteDTO(clientePacote.getCliente());
        this.pacote = new PacoteDTO(clientePacote.getPacote());
        this.quantidade = clientePacote.getQuantidade();
    }

    public Cliente_X_PacoteDTO(){

    }

    public Cliente_X_PacoteEntity converter(){
        Cliente_X_PacoteEntity clientePacote = new Cliente_X_PacoteEntity();
        clientePacote.setId(this.id);
        clientePacote.setCliente(this.getCliente() != null ? this.cliente.converter() : null);
        clientePacote.setPacote(this.getPacote() != null ? this.pacote.converter() : null);
        clientePacote.setQuantidade(this.quantidade);
        return clientePacote;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
