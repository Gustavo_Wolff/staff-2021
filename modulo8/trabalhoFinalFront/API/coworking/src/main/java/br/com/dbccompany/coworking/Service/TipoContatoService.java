package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.TipoContato.ErroAoSalvarTipoContato;
import br.com.dbccompany.coworking.Exception.TipoContato.TipoContatoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContatoDTO salvar(TipoContatoEntity tipoContato) throws ErroAoSalvarTipoContato {
        try{
            return new TipoContatoDTO(repository.save(tipoContato));
        }catch (Exception e){
            throw new ErroAoSalvarTipoContato();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContatoDTO editar(TipoContatoEntity tipoContato, Integer id) throws ErroAoSalvarTipoContato {
        tipoContato.setId(id);
        return salvar(tipoContato);
    }

    private List<TipoContatoDTO> listaEntityParaDBO (List<TipoContatoEntity> listaEntity){
        List<TipoContatoDTO> listaDTO = new ArrayList<>();
        for (TipoContatoEntity elem : listaEntity){
            listaDTO.add(new TipoContatoDTO(elem));
        }
        return listaDTO;
    }

    public List<TipoContatoDTO> buscarTodos() throws TipoContatoNaoEncontrado {
        return listaEntityParaDBO(repository.findAll());
    }

    public TipoContatoDTO buscarPorId(Integer id) throws TipoContatoNaoEncontrado {
        Optional<TipoContatoEntity> tipoContato = repository.findById(id);
        if(tipoContato.isPresent()){
            return new TipoContatoDTO(tipoContato.get());
        }
        throw new TipoContatoNaoEncontrado();
    }

}
