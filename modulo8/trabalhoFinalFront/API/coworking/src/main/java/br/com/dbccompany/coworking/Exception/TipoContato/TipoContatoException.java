package br.com.dbccompany.coworking.Exception.TipoContato;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class TipoContatoException extends GeneralException {

    public TipoContatoException (String mensagem){
        super(mensagem);
    }
}
