package br.com.dbccompany.coworking.Exception.Acesso;

public class NenhumAcessoParaSaldoBuscado extends AcessoException {

    public NenhumAcessoParaSaldoBuscado(){
        super("O saldo buscado não possui nenhum acesso!");
    }
}
