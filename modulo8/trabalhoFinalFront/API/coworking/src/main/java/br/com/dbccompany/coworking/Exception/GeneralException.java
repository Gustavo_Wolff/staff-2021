package br.com.dbccompany.coworking.Exception;

public class GeneralException extends Exception {

    private String mensagem;

    public GeneralException(String mensagem){
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
