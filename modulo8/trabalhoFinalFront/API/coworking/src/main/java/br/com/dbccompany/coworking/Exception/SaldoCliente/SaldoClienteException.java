package br.com.dbccompany.coworking.Exception.SaldoCliente;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class SaldoClienteException extends GeneralException {

    public SaldoClienteException (String mensagem){
        super(mensagem);
    }
}
