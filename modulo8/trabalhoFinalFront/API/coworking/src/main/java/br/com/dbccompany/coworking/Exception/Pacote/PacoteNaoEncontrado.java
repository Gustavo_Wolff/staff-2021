package br.com.dbccompany.coworking.Exception.Pacote;

public class PacoteNaoEncontrado extends PacoteException {
    public PacoteNaoEncontrado(){
        super("Nenhum pacote foi encontrado!");
    }
}
