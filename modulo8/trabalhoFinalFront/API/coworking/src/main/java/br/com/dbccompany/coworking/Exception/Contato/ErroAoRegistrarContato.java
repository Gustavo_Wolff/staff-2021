package br.com.dbccompany.coworking.Exception.Contato;

public class ErroAoRegistrarContato extends ContatoException{
    public ErroAoRegistrarContato(){
        super("Não foi possível registrar o contato solicitado!");
    }
}
