package br.com.dbccompany.coworking.Exception.Espaco;

public class ErroAoRegistrarEspaco extends EspacoException {
    public ErroAoRegistrarEspaco(){
        super("Não foi possível registrar o espaço solicitado!");
    }
}
