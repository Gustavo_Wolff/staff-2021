package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity {

    @Id
    @SequenceGenerator(name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    private SaldoClienteEntity saldoCliente;

    private Boolean entrada;

    private LocalDateTime data;

    private Boolean excessao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getExcessao() {
        return excessao;
    }

    public void setExcessao(Boolean excessao) {
        this.excessao = excessao;
    }
}
