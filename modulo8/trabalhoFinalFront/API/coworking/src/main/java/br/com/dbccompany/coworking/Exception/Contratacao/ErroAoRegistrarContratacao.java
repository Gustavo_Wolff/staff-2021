package br.com.dbccompany.coworking.Exception.Contratacao;

public class ErroAoRegistrarContratacao extends ContratacaoException{
    public ErroAoRegistrarContratacao(){
        super("Não foi possível registrar a contratação solicitada!");
    }
}
