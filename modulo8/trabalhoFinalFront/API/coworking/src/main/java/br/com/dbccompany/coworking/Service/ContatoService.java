package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.Contato.ErroAoRegistrarContato;
import br.com.dbccompany.coworking.Exception.Contato.NenhumContatoEncontrado;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ContatoDTO salvar(ContatoEntity contato) throws ErroAoRegistrarContato {
        try{
            return new ContatoDTO(repository.save(contato));
        }catch (Exception e){
            throw new ErroAoRegistrarContato();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ContatoDTO editar(ContatoEntity contato, Integer id) throws ErroAoRegistrarContato {
        try{
            contato.setId(id);
            return salvar(contato);
        }catch (Exception e){
            throw new ErroAoRegistrarContato();
        }
    }

    private List<ContatoDTO> converterEntityParaDTO(List<ContatoEntity> contatos){
        List<ContatoDTO> dtos = new ArrayList<>();
        for(ContatoEntity contato : contatos){
            dtos.add(new ContatoDTO(contato));
        }
        return dtos;
    }

    public List<ContatoDTO> buscarTodos() throws NenhumContatoEncontrado {
        try{
            return converterEntityParaDTO(repository.findAll());
        }catch (Exception e){
            throw new NenhumContatoEncontrado();
        }
    }

    public ContatoDTO buscarPorId(Integer id) throws NenhumContatoEncontrado {
        Optional<ContatoEntity> contato = repository.findById(id);
        if(contato.isPresent()){
            return new ContatoDTO(contato.get());
        }
        throw new NenhumContatoEncontrado();
    }

    public List<ContatoDTO> buscarPorTipoContato(TipoContatoEntity tipoContato) throws NenhumContatoEncontrado {
        try{
            List<ContatoEntity> contatos = repository.findAllByTipoContato(tipoContato);
            return converterEntityParaDTO(contatos);
        }catch (Exception e){
            throw new NenhumContatoEncontrado();
        }
    }

}
