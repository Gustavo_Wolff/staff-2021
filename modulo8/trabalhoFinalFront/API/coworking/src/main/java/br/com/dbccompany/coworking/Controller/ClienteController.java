package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteInformadoNaoEncontrado;
import br.com.dbccompany.coworking.Exception.Cliente.ErroAoSalvarCliente;
import br.com.dbccompany.coworking.Exception.Cliente.NenhumClienteRegistrado;
import br.com.dbccompany.coworking.Service.ClienteService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ClienteDTO> salvar (@RequestBody ClienteDTO cliente){
        try{
            return new ResponseEntity<>(service.salvar(cliente.converter()), HttpStatus.CREATED);
        } catch(ErroAoSalvarCliente e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ClienteDTO> editar (@RequestBody ClienteDTO cliente, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(cliente.converter(), id), HttpStatus.CREATED);
        } catch(ErroAoSalvarCliente e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ClienteDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch (NenhumClienteRegistrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ClienteDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch(ClienteInformadoNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
