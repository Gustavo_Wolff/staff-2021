package br.com.dbccompany.coworking.Exception.Contato;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class ContatoException extends GeneralException {

    public ContatoException (String mensagem){
        super(mensagem);
    }
}
