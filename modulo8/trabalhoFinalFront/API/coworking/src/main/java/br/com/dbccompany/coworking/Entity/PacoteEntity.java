package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity {

    @Id
    @SequenceGenerator(name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private Double valor;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Espaco_X_PacoteEntity> espacoPacotes;

    @OneToMany(mappedBy = "pacote")
    private List<Cliente_X_PacoteEntity> clientePacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Espaco_X_PacoteEntity> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<Espaco_X_PacoteEntity> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    public List<Cliente_X_PacoteEntity> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<Cliente_X_PacoteEntity> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }
}