package br.com.dbccompany.coworking.Exception.TipoContato;

public class TipoContatoNaoEncontrado extends TipoContatoException {
    public TipoContatoNaoEncontrado(){
        super("Nenhum tipo de contato encontrado!");
    }
}
