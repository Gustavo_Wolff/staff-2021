package br.com.dbccompany.coworking.Exception.Pacote;

public class ErroAoRegistrarPacote extends PacoteException {
    public ErroAoRegistrarPacote(){
        super("Não foi possível registrar o pacote solicitado!");
    }
}
