package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {

    List<ClienteEntity> findAll();
    Optional<ClienteEntity> findById(Integer id);
    List<ClienteEntity> findAllById(Iterable<Integer> ids);

    Optional<ClienteEntity> findByNome(String nome);
    List<ClienteEntity> findAllByNome(String nome);

    Optional<ClienteEntity> findByCpf(Character[] cpf);

    Optional<ClienteEntity> findByDataNascimento(LocalDate dataNascimento);
    List<ClienteEntity> findAllByDataNascimento(LocalDate dataNascimento);


}
