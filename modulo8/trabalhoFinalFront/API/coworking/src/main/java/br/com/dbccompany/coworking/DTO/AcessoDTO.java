package br.com.dbccompany.coworking.DTO;


import br.com.dbccompany.coworking.Entity.AcessoEntity;

import java.time.LocalDateTime;

public class AcessoDTO {

    private Integer id;
    private SaldoClienteDTO saldoCliente;
    private Boolean entrada;
    private LocalDateTime data;
    private Boolean excessao = false;
    private String mensagem;

    public AcessoDTO(AcessoEntity acesso){
        this.id = acesso.getId();
        this.saldoCliente = new SaldoClienteDTO(acesso.getSaldoCliente());
        this.entrada = acesso.getEntrada();
        this.data = acesso.getData();
        this.excessao = acesso.getExcessao();
    }

    public AcessoDTO(){

    }

    public AcessoEntity converter(){
        AcessoEntity acesso = new AcessoEntity();
        acesso.setId(this.id);
        acesso.setSaldoCliente(this.getSaldoCliente() != null ? this.saldoCliente.converter() : null);
        acesso.setEntrada(this.entrada);
        acesso.setData(this.data);
        acesso.setExcessao(this.excessao);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteDTO getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteDTO saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getExcessao() {
        return excessao;
    }

    public void setExcessao(Boolean excessao) {
        this.excessao = excessao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
