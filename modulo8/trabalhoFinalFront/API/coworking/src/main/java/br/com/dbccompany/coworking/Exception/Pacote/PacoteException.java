package br.com.dbccompany.coworking.Exception.Pacote;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class PacoteException extends GeneralException {

    public PacoteException (String mensagem){
        super(mensagem);
    }
}
