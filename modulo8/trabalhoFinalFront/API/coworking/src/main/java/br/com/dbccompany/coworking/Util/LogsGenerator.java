package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.Exception.GeneralException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class LogsGenerator {

    public static Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    public static RestTemplate restTemplate = new RestTemplate();

    public static String url = "http://localhost:8081/logs/salvar";


    public static void erro422 (GeneralException e){
        logger.error(e.getMensagem());
        restTemplate.postForObject(url, new ErroDTO(e, "ERROR", "422"), Object.class);
        System.err.println(e.getMensagem());
    }

    public static void erro404(GeneralException e){
        logger.error(e.getMensagem());
        restTemplate.postForObject(url, new ErroDTO(e, "ERROR", "404"), Object.class);
        System.err.println(e.getMensagem());

    }
}
