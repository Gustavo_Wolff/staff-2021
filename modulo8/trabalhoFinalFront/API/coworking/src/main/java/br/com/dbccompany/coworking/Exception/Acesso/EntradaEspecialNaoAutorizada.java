package br.com.dbccompany.coworking.Exception.Acesso;

public class EntradaEspecialNaoAutorizada extends AcessoException{

    public EntradaEspecialNaoAutorizada(){
        super("Não foi possível concluir entrada na contratação Especial!");
    }
}
