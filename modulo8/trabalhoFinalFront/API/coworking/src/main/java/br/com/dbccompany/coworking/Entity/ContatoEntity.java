package br.com.dbccompany.coworking.Entity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import javax.persistence.*;

@Entity
public class ContatoEntity {

    @Id
    @SequenceGenerator( name = "CONTACTO_SEQ", sequenceName = "CONTACTO_SEQ")
    @GeneratedValue( generator = "CONTACTO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private TipoContatoEntity tipoContato;

    private String contato;

    @ManyToOne
    private ClienteEntity cliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}