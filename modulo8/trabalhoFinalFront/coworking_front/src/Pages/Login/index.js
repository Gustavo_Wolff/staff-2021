import React from "react";

import { Link } from 'react-router-dom';

import FormLogin from '../../Midware/FormLogin'

import {  Row, Col } from 'antd';

import AuthenticationApi from "../../API/AuthenticationApi";
import Usuario from "../../Models/Usuario";

import '../Pages.css';
import BotaoAzul from "../../Midware/BotaoAzul";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.api = new AuthenticationApi();
    this.state = {
      mensagem: '',
      exibirAcesso: false
    }
  }

  fazerLogin( valores ) {
    const usuario = new Usuario(valores);
    this.api.login(usuario).then(res => {
      localStorage.setItem('token', res.headers.authorization);
      this.autenticacaoBemSucessida();
      setTimeout(() => {
        this.setState(state => {
          return {
            ...state,
            exibirAcesso: true
          }
        })
      }, 3000)
    }).catch(() => {
      this.usuarioInvalido();
    });
  }

  autenticacaoBemSucessida() {
    this.setState(state => {
      return {
        ...state,
        mensagem: "Usuário autenticado! Aguarde..."
      }
    });
    this.removerMensagem();
  }

  usuarioInvalido() {
    this.setState(state => {
      return {
        ...state,
        mensagem: "Usuário inválido!"
      }
    });
    this.removerMensagem();
  }

  removerMensagem() {
    setTimeout(() => {
      this.setState(state => {
        return {
          ...state,
          mensagem: ''
        }
      })
    }, 3000);
  }

  render() {
    return (
      <div className='general' >
        <Row className='background-login' >
          <Col span={ 24 } >
            <FormLogin className=''  onFinish={this.fazerLogin.bind( this )} />    
          </Col>
        </Row>
        <Row type="flex" align="middle" justify="center"  >
          <Col span={ 24 } className='text-align' >
            <h3>{this.state.mensagem}</h3>
            {this.state.exibirAcesso ? <BotaoAzul nome='Acessar Aplicação' endereco='/home' /> : ''}
          </Col>
        </Row>
      </div>
    )
  }
}