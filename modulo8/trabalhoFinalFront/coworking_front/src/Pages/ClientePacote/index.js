import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import FormClientePacote from '../../Midware/FormClientePacote';
import CampoBusca from '../../Midware/CampoBusca';
import ApiSalvar from '../../API/ApiSalvar';
import ApiBuscar from '../../API/ApiBuscar';
import ClientePacoteModel from '../../Models/ClientePacoteModel';
import Tabela from '../../Midware/Tabela';

export default class Home extends React.Component {

  constructor(props) {
    super(props);
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: '',
      especifica: '',
      mensagem: ''
    }
  }

  componentDidMount() {
    this.buscarTodos();
  }

  buscarTodos() {
    this.apiBuscar.buscarClientePacotes().then(res => {
      const todosFormatado = res.map(cp => {
        return {
          ...cp,
          cliente: cp.cliente.id,
          pacote: cp.pacote.id,
          valor: cp.pacote.valor,
          nomeCliente: cp.cliente.nome
        }
      })
      console.log( todosFormatado )
      this.setState(state => {
        return {
          ...state,
          todos: todosFormatado
        }
      })
    })
  }

  buscarPorId(id) {
    this.apiBuscar.buscarClientePacotePorId(id.id).then(res => {
      this.setState(state => {
        return {
          ...state,
          especifica: res
        }
      })
    })
  }

  registrarClientePacote(data) {
    if( data.quantidade === undefined || data.quantidade === '' ){
      data.quantidade = 1
    }
    let idCliente = new Object();
    idCliente.id = data.cliente;
    let idPacote = new Object();
    idPacote.id = data.pacote;
    const cpNovo = new ClientePacoteModel(idCliente, idPacote, data.quantidade);
    data.id === undefined
      ? this.salvarClientePacote(cpNovo)
      : this.editarClientePacote(cpNovo, data.id);
  }

  salvarClientePacote(cp) {
    this.apiSalvar.salvarClientePacote(cp).then(res => {
      this.buscarTodos();
    }).catch(() => {
      this.dadosInvalidos()
    })
  }

  editarClientePacote(cp, id) {
    this.apiSalvar.editarClientePacote(cp, id).then(res => {
      console.log( 'entrou' )
      this.buscarTodos()
    }).catch(() => {
      this.dadosInvalidos()
    })
  }

  dadosInvalidos() {
    this.setState(state => {
      return {
        ...state,
        mensagem: 'Dados rejeitados pelo servidor. \n Verifique-os e tente novamente.',
      }
    });
    setTimeout(() => {
      this.setState(state => {
        return {
          ...state,
          mensagem: ''
        }
      })
    }, 5000)
  }



  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Contratação de Pacotes' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col>
            <Col span={18} className='tirar-do-header text-align inline-block' >
              <Row>
                <Col span={13} >
                  <FormClientePacote salvar={this.registrarClientePacote.bind(this)} />
                  <h3 className='vermelho text-align' >{this.state.mensagem}</h3>
                </Col>
                <Col className='margin-left-50' >
                  <CampoBusca metodo={this.buscarPorId.bind(this)} titulo={ `Buscar Detalhes de Contratação` } />
                  {this.state.especifica ? (

                    <div className='background-salmon' >
                      <h3>Dados da Contratação de Pacote</h3>
                      <p>{`ID: ${this.state.especifica.id}`}</p>
                      <p>{`Quantidade: ${this.state.especifica.quantidade}`}</p>
                      <h4>Pacote:</h4>
                      <p>{`ID: ${this.state.especifica.pacote.id}`}</p>
                      <p>{`Valor ${this.state.especifica.pacote.valor}`}</p>
                      <h4>Cliente:</h4>
                      <p>{`ID: ${this.state.especifica.cliente.id}`}</p>
                      <p>{`Nome: ${this.state.especifica.cliente.nome} / CPF: ${this.state.especifica.cliente.cpf}`}</p>
                    </div>
                  ) : ''}
                </Col>
              </Row>
              <Row className='container margin-20' >
                <Col span={ 18 } >
                  <Tabela dataSource={this.state.todos} colunas={[
                    {
                      title: 'ID',
                      dataIndex: 'id',
                      key: 'id'
                    },
                    {
                      title: 'ID Cliente',
                      dataIndex: 'cliente',
                      key: 'cliente'
                    },
                    {
                      title: 'Nome Cliente',
                      dataIndex: 'nomeCliente',
                      key: 'nomeCliente'
                    },
                    {
                      title: 'ID Pacote',
                      dataIndex: 'pacote',
                      key: 'pacote'
                    },
                    {
                      title: 'Preço Unitário',
                      dataIndex: 'valor',
                      key: 'valor'
                    },
                    {
                      title: 'Quantidade',
                      dataIndex: 'quantidade',
                      key: 'quantidade'
                    }
                  ]} />
                </Col>
              </Row>
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}