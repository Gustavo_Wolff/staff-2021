import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import BotaoManejar from '../../Midware/BotaoManejar';
import TabelaPacotes from '../../Midware/TabelaPacotes';
import ApiBuscar from '../../API/ApiBuscar'

export default class Pacote extends React.Component {

  constructor( props ){
    super( props );
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: '',
    }
  }

  componentDidMount(){
    this.buscarTodos();
  }

  buscarTodos(){
    this.apiBuscar.buscarPacotes().then( pacotes => {
      const pacotesFormatados = this.adicionarKeys( pacotes );
      this.setState( state => {
        return{
          ...state,
          todos: pacotesFormatados
        }
      } )
    } )
  }

  adicionarKeys( pacotes ){
    return pacotes.map( pacote => {
      return{
        ...pacote,
        key: pacote.id
      }
    } )
  }

  render(){
    return(
      <div className="general">
        <header>
        <Cabecalho nomePagina='Pacotes' />
          <Row>
            <Col span={ 3 } className='nav' >
              <Navigator />
            </Col>
            <Col className='tirar-do-header margin-left-50' span={14} >
            <BotaoManejar endereco='cadastro-pacote' nome='Manejar Pacotes' />
            <TabelaPacotes data={ this.state.todos } />
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}