import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import Tabela from '../../Midware/Tabela';
import ApiBuscar from '../../API/ApiBuscar';
import ApiSalvar from '../../API/ApiSalvar';
import MeuSwitch from '../../Midware/MeuSwitch';
import FormAcesso from '../../Midware/FormAcesso';

export default class Acesso extends React.Component {
  constructor(props) {
    super(props);
    this.apiBuscar = new ApiBuscar();
    this.apiSalvar = new ApiSalvar();
    this.state = {
      todos: '',
      atual: '',
      saldoInsuficiente: '',
      isEntrada: true,
      isEntradaEspecial: false,
      mensagem: ''
    }
  }

  componentDidMount() {
    this.buscarTodos();
  }

  buscarTodos() {
    this.apiBuscar.buscarAcessos().then(res => {
      const acessosFormatado = this.formatarAcessos(res);
      this.setState(state => {
        return {
          ...state,
          todos: acessosFormatado
        }
      })
    })
  }

  formatarAcessos(acessos) {

    return acessos.map(ac => {
      let dataAcesso = this.formatarDataEntrada(ac.data);
      return this.formatarUmAcesso( ac, dataAcesso );
    })
  }

  formatarUmAcesso( ac, dataAcesso ) {
    return {
      id: ac.id,
      clienteId: ac.saldoCliente.id_cliente,
      espacoId: ac.saldoCliente.id_espaco,
      clienteNome: ac.saldoCliente.cliente.nome,
      espacoNome: ac.saldoCliente.espaco.nome,
      tipoContratacao: ac.saldoCliente.tipoContratacao,
      quantidade: ac.saldoCliente.quantidade,
      vencimento: this.formatarData(ac.saldoCliente.vencimento),
      entrada: ac.entrada ? 'Entrada' : 'Saída',
      data: this.formatarData(dataAcesso[0]),
      hora: dataAcesso[1]
    }
  }

  formatarData(data) {
    return data.split('-').reverse().join('/');
  }

  formatarDataEntrada(data) {
    return data.split('.')[0].split('T')
  }

  registrarAcesso(acesso) {
    const acessoFormatado = this.construirAcesso(acesso);
    this.state.isEntrada ? this.verificarTipoEntrada(acessoFormatado) : this.salvarSaida(acessoFormatado);
  }

  verificarTipoEntrada(acesso) {
    this.state.isEntradaEspecial ? this.salvarEntradaEspecial(acesso) : this.salvarEntrada(acesso);
  }

  salvarEntrada(acesso) {
    this.apiSalvar.salvarEntrada(acesso).then( res => {
      this.ExibirAcessoSalvo( res )
      this.buscarTodos();
    }).catch( () => {
      this.dadosInvalidos();
    } );
  }

  salvarEntradaEspecial( acesso ){
    this.apiSalvar.salvarEntradaEspecial( acesso ).then( res => {
      this.ExibirAcessoSalvo( res )
      this.buscarTodos();
    }).catch( () => {
      this.dadosInvalidos();
    } );
  }

  salvarSaida( acesso ){
    this.apiSalvar.salvarSaida( acesso ).then( res => {
      this.ExibirAcessoSalvo( res )
      this.buscarTodos();
    }).catch( () => {
      this.dadosInvalidos();
    } );
  }

  ExibirAcessoSalvo( acesso ){
    if ( acesso.id !== null ){
      this.setState( state => {
        return{
          ...state,
          atual: acesso,
          saldoInsuficiente: ''
        }
      } )
    } else{
      this.setState( state => {
        return{
          ...state,
          saldoInsuficiente: acesso,
          atual: ''
        }
      } )
    }  
  }

  construirAcesso(acesso) {
    let saldoCliente = new Object();
    saldoCliente.id_cliente = acesso.id_cliente;
    saldoCliente.id_espaco = acesso.id_espaco;
    let acessoFormatado = new Object();
    acessoFormatado.saldoCliente = saldoCliente;
    return acessoFormatado;
  }

  estadoEntradaOuSaida(estado) {
    this.setState(state => {
      return {
        ...state,
        isEntrada: estado
      }
    })

  }

  estadoEntradaEspecial(estado) {
    this.setState(state => {
      return {
        ...state,
        isEntradaEspecial: estado
      }
    })
  }

  dadosInvalidos() {
    this.setState(state => {
      return {
        ...state,
        mensagem: 'Dados rejeitados pelo servidor. \n Verifique-os e tente novamente.',
      }
    });
    setTimeout(() => {
      this.setState(state => {
        return {
          ...state,
          mensagem: ''
        }
      })
    }, 5000)
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Acessos' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col>
            <Col span={18} className='tirar-do-header text-align inline-block' >
              <Row>
                <Col span={8} >
                  <h1>Processar Acesso</h1>
                  <p>1 - Informe se o acesso trata-se de uma entrada ou de uma saída.</p>
                  <p>2 - Caso seja uma entrada, informe se esta é Regular (referente a uma contratação) ou Especial (entrada avulsa).</p>
                  <p>3 - Informe os IDs referentes ao acesso.</p>
                  <MeuSwitch
                    textoTrue='Entrada'
                    textoFalse='Saída'
                    estadoPadrao={true}
                    pegar={this.estadoEntradaOuSaida.bind(this)}
                  />
                  <MeuSwitch
                    textoTrue='Entrada Especial'
                    textoFalse='Entrada Regular'
                    estadoPadrao={false}
                    pegar={this.estadoEntradaEspecial.bind(this)}
                  />
                  <FormAcesso salvar={this.registrarAcesso.bind(this)} />
                  <h4 className='vermelho margin-left-20' >{ this.state.mensagem }</h4>
                </Col>
                <Col span={ 15 } >
                { this.state.atual ? (
                    <div className='background-salmon margin-left-50 ' >
                      <h3>Dados do Acesso</h3>
                      <p>ID: { this.state.atual.id }</p>
                      <p>ID Cliente: { this.state.atual.saldoCliente.id_cliente } / ID Espaço: { this.state.atual.saldoCliente.id_espaco }</p>
                      <p>Data de Acesso: { this.state.atual.data }</p>
                      <p>Nome Cliente: { this.state.atual.saldoCliente.cliente.nome }</p>
                      <p>Espaço: { this.state.atual.saldoCliente.espaco.nome }</p>
                      <p>Tipo de Contratação: { this.state.atual.saldoCliente.tipoContratacao } / Crédito: { this.state.atual.saldoCliente.quantidade }</p>
                      <p>Vencimento: { this.formatarData( this.state.atual.saldoCliente.vencimento ) }</p>
                      <p>Entrada/Saída: { this.state.atual.entrada ? 'Entrada' : 'Saída' }</p>
                    </div>
                  ) : '' }
                  { this.state.saldoInsuficiente ? (
                    <div className='background-salmon margin-left-50 ' >
                      <h3>{ this.state.saldoInsuficiente.mensagem }</h3>
                      <h4>Há duas opções:</h4>
                      <h4>1 - Acrescente crédito realizando o pagamento de uma Contratação.</h4>
                      <h4>2 - Acesse com Entrada Especial (validade de apenas 1 dia).</h4>
                    </div>
                  ) : '' }
                </Col>
              </Row>
              <Row className='container display-block ' >
                <h2>Registro de acessos</h2>
                <Tabela dataSource={this.state.todos} colunas={[
                  {
                    title: 'ID',
                    dataIndex: 'id',
                    key: 'id'
                  },
                  {
                    title: 'ID Cliente',
                    dataIndex: 'clienteId',
                    key: 'clienteId'
                  },
                  {
                    title: 'ID Espaço',
                    dataIndex: 'espacoId',
                    key: 'espacoId'
                  },
                  {
                    title: 'Nome Cliente',
                    dataIndex: 'clienteNome',
                    key: 'clienteNome'
                  },
                  {
                    title: 'Espaço',
                    dataIndex: 'espacoNome',
                    key: 'espacoNome'
                  },
                  {
                    title: 'Tipo De Contratação',
                    dataIndex: 'tipoContratacao',
                    key: 'tipoContratacao'
                  },
                  {
                    title: 'Crédito',
                    dataIndex: 'quantidade',
                    key: 'quantidade'
                  },
                  {
                    title: 'Vencimento',
                    dataIndex: 'vencimento',
                    key: 'vencimento'
                  },
                  {
                    title: 'Entrada/Saída',
                    dataIndex: 'entrada',
                    key: 'entrada'
                  },
                  {
                    title: 'Data de Acesso',
                    dataIndex: 'data',
                    key: 'data'
                  },
                  {
                    title: 'Hora de Acesso',
                    dataIndex: 'hora',
                    key: 'hora'
                  }
                ]} />
              </Row>
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}
