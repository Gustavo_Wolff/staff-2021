import React from "react";

import { Row, Col } from "antd";

import '../Pages/Pages.css'

const Cabecalho = props => {

  const { nomePagina } = props;

  return (
    <div className='header' >
      <Row>
        <Col span={ 18 }>
          <h1 className='titulo'>Coworking</h1>
        </Col>
        <Col>
          <h2 className='nome-pagina' >{nomePagina}</h2>
        </Col>
      </Row>


    </div>
  )
}

export default Cabecalho;