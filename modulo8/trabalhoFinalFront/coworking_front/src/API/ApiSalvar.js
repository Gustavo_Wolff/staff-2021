import axios from "axios";

export default class ApiSalvar{
  constructor(){
    this.url = 'http://localhost:8080/cw';
    this.token = localStorage.getItem( 'token' );
    this.config = { headers: { Authorization: this.token } };
  }

  salvarTipoContato( tipoContato ){
    return axios.post( `${ this.url }/tipoContato/salvar`, tipoContato, this.config ).then( res => res.data );
  };

  editarTipoContato( tipoContato, id ){
    return axios.put( `${ this.url }/tipoContato/editar/${ id }`, tipoContato, this.config ).then( res => res.data );
  }

  salvarCliente( cliente ){
    return axios.post( `${ this.url }/cliente/salvar`, cliente, this.config ).then( res => res.data );
  }

  editarCliente( cliente, id ){
    return axios.put( `${ this.url }/cliente/editar/${ id }`, cliente, this.config  ).then( res => res.data );
  }

  salvarEspaco( espaco ){
    return axios.post( `${ this.url }/espaco/salvar`, espaco, this.config ).then( res => res.data );
  }

  editarEspaco( espaco, id ){
    return axios.put( `${ this.url }/espaco/editar/${ id }`, espaco, this.config ).then( res => res.data );
  }

  salvarPacote( pacote ){
    return axios.post( `${ this.url }/pacote/salvar`, pacote, this.config ).then( res => res.data );
  }

  editarPacote( pacote, id ){
    return axios.put( `${ this.url }/pacote/editar/${ id }`, pacote, this.config ).then( res => res.data );
  }

  salvarContratacao( contratacao ){
    return axios.post( `${ this.url }/contratacao/salvar`, contratacao, this.config ).then( res => res.data );
  }

  editarContratacao( contratacao, id ){
    return axios.put( `${ this.url }/contratacao/editar/${ id }`, contratacao, this.config ).then( res => res.data );
  }

  salvarClientePacote( clientePacote ){
    return axios.post( `${ this.url }/clientePacote/salvar`, clientePacote, this.config ).then( res => res.data );
  }

  editarClientePacote( clientePacote, id ){
    return axios.put( `${ this.url }/clientePacote/editar/${ id }`, clientePacote, this.config ).then( res => res.data );
  }

  salvarPagamento( pagamento ){
    return axios.post( `${ this.url }/pagamento/pagar`, pagamento, this.config ).then( res => res.data );
  }

  salvarEntrada( entrada ){
    return axios.post( `${ this.url }/acesso/entrar`, entrada, this.config ).then( res => res.data );
  }

  salvarSaida( saida ){
    return axios.post( `${ this.url }/acesso/sair`, saida, this.config ).then( res => res.data );
  }

  salvarEntradaEspecial( entrada ){
    return axios.post( `${ this.url }/acesso/entradaEspecial`, entrada, this.config ).then( res => res.data );
  }




  // salvarContratacao( { espaco, cliente, tipoContratacao, quantidade, prazo } );

  // salvarPagamento( { contratacaoOuClientePacote, tipoPagamento } );

  // salvarEntrada( { saldoCliente } );

  // salvarSaida( { saldoCliente } );
}