import { Button, Row, Col } from 'antd';

const BotaoManejar = props => {

  const { nome, metodo, endereco } = props;

  return (
    <Row>
      <Col span={24} ghost className='botao-manejar' >
        <Button size='large' danger className='botao' href={ endereco }  onClick={ metodo } type="primary" >{nome}</Button>
      </Col>
    </Row>

  )
}

export default BotaoManejar;