import { Button, Row, Col } from 'antd';

const BotaoAzul = props => {

  const { nome, metodo, endereco, laranja } = props;

  return (
    <Row>
      <Col span={24}>
        <Button className='botao' href={ endereco }  onClick={ metodo } type="primary" >{nome}</Button>
      </Col>
    </Row>

  )
}

export default BotaoAzul;