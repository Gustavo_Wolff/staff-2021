import { Form, Input, Button } from 'antd';

const CampoBusca = props => {
  const{ metodo, titulo } = props;

  return(
    <div>
      <h2 type='flex' align='middle' justify='center'>{ titulo }</h2>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 12 }}
        wrapperCol={{ span: 6 }}
        onFinish={metodo}
      >

        <Form.Item
          label='ID'
          name="id"
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 12,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Buscar
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default CampoBusca;