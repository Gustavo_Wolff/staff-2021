import { Table, Badge, Menu, Dropdown, Space } from 'antd';
import { DownOutlined } from '@ant-design/icons';

const menu = (
  <Menu>
    <Menu.Item>Action 1</Menu.Item>
    <Menu.Item>Action 2</Menu.Item>
  </Menu>
);

const TabelaClientes = props => {

  let { data } = props;


  const expandedRowRender = row => {
    const columns = [
      { title: 'Tipo de Contato', dataIndex: 'tipoContato', key: 'tipoContato' },
      { title: 'Contato', dataIndex: 'contato', key: 'contato' },
    ];

    const data = row.contatos.map( contato => {
      return {
         contato: contato.contato,
         tipoContato: contato.tipoContato.nome }} );
  
    return <Table columns={columns} dataSource={data} pagination={false} />;
  };

  const columns = [
    { title: 'ID', dataIndex: 'id', key: 'id' },
    { title: 'Nome', dataIndex: 'nome', key: 'nome' },
    { title: 'CPF', dataIndex: 'cpf', key: 'cpf' },
    { title: 'Data de Nascimento', dataIndex: 'dataNascimento', key: 'dataNascimento' }
  ];

  return (
    <Table
      className="components-table-demo-nested margin-left-20"
      columns={columns}
      expandable={{ expandedRowRender }}
      dataSource={data}
    />
  );
}

export default TabelaClientes;

