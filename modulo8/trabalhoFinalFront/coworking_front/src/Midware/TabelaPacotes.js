import { Table, Badge, Menu, Dropdown, Space } from 'antd';
import { DownOutlined } from '@ant-design/icons';

const menu = (
  <Menu>
    <Menu.Item>Action 1</Menu.Item>
    <Menu.Item>Action 2</Menu.Item>
  </Menu>
);

const TabelaPacotes = props => {

  let { data } = props;



  const expandedRowRender = row => {
    const columns = [
      { title: 'ID Espaço', dataIndex: 'espacoId', key: 'espacoId' },
      { title: 'Nome Espaço', dataIndex: 'espacoNome', key: 'espacoNome' },
      { title: 'Tipo de Contratação', dataIndex: 'tipoContratacao', key: 'tipoContratacao' },
      { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade' },
      { title: 'Prazo', dataIndex: 'prazo', key: 'prazo' }
    ];

    const data = row.espacoPacote.map(ep => {
      return {
        espacoId: ep.espaco.id,
        espacoNome: ep.espaco.nome,
        tipoContratacao: ep.tipoContratacao,
        quantidade: ep.quantidade,
        prazo: ep.prazo
      }
    });

    return <Table columns={columns} dataSource={data} pagination={false} />;
  };

  const columns = [
    { title: 'ID', dataIndex: 'id', key: 'id' },
    { title: 'Valor', dataIndex: 'valor', key: 'valor' }
  ];

  return (
    <Table
      className="components-table-demo-nested margin-left-200"
      columns={columns}
      expandable={{ expandedRowRender }}
      dataSource={data}
    />
  );
}

export default TabelaPacotes;