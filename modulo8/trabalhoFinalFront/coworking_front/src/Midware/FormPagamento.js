import { Form, Input, Button } from 'antd';

const FormPagamento = props => {

  const { salvar } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Processar Pagamento</h1>
      <p>Preencha com o ID apenas o campo correspondente à modalidade de contratação a ser paga. Deixe o outro campo em BRANCO.</p>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 12 }}
        onFinish={salvar}
      >

        <Form.Item
          label='ID Contratação Individual'
          name="contratacao"
        >
          <Input placeholder='OPCIONAL' />
        </Form.Item>
        <Form.Item
          label='ID Contratação Pacote'
          name="clientePacote"
        >
          <Input placeholder='OPCIONAL' />
        </Form.Item>
        <Form.Item
          label='Tipo de Pagamento'
          name="tipoPagamento"
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Realizar Pagamento 
          </Button>
        </Form.Item>
      </Form>
    </div>

  )
}

export default FormPagamento;