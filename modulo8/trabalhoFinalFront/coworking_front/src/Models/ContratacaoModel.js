export default class ContratacaoModel{
  constructor( espaco, cliente, tipoContratacao, quantidade, desconto, prazo, id ){
    this.id = id ? id : '';
    this.espaco = espaco;
    this.cliente = cliente;
    this.tipoContratacao = tipoContratacao;
    this.quantidade = quantidade;
    this.desconto = desconto;
    this.prazo = prazo;
  }
}