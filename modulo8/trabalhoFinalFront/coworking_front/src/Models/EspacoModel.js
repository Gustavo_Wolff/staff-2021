export default class EspacoModel{
  constructor( nome, qntPessoas, valor, id ){
    this.id = id ? id : '';
    this.nome = nome;
    this.qntPessoas = qntPessoas;
    this.valor = valor;
  }
}