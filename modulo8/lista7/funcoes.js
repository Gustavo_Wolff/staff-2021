/** Exercicio-2  */
Array.prototype.filtrarPorAno = function (ano) {
    let arraysFiltrados = [];
    for (let index = 0; index < this.length; index++ ) {
        if (this[index].anoEstreia >= ano) {
            arraysFiltrados.push(this[index]);
        }
    }
    console.log(arraysFiltrados);
    return arraysFiltrados;
}


/** Exercicio-3 *//*
Array.prototype.procurarPorNome = function (nome) {
    let nomeElenco = false;
    for (let index 0; index < this.length; index++) {
        if (this.[index].nome === nome) {
            nomeElenco = true;
        }
    }
    return nomeElenco;
}*/

Array.prototype.procurarPorNome = function ( nome ) {
    let nomesArray = this.filter( serie => serie.elenco.includes(nome));
    return nomesArray.length > 0 ? true : false;
}

console.log(`Exercicio 3 - ${ series.procurarPorNome( `David Harbour`) }` );

/** Exercicio-4 */

Array.prototype.mediaDeEpisodios = function() {

    let totalEpisodios = 0;
  
    for (let index = 0; index < this.length; index++) {
      totalEpisodios += this[index].numeroEpisodios;
    }
  
    return totalEpisodios / this.length;
  }
  
  console.log(`Exercicio 4 - ${ series.mediaDeEpisodios() }` );
  
/** Exercicio-5 */
  
  Array.prototype.totalSalarios = function( indice ) {
  
    let totalDosSalarios = this[indice].diretor.length * 100000 + this[indice].elenco.length * 40000;
  
    return totalDosSalarios;
  }
  
  console.log(`Exercicio 5 - ${ series.totalSalarios(0) }` );
  
/** Exercicio-6A */
  
  Array.prototype.queroGenero = function(genero) {
  
    const generoArray = this.filter( serie => serie.genero.includes( genero ));
    const titulosDasSeries = generoArray.map( element => element.titulo )
  
    return titulosDasSeries;
  }
  
  console.log(`Exercicio 6 A - ${ series.queroGenero( 'Caos' ) }` );
  
 /** Exercicio-6B */
  
  Array.prototype.queroTitulo = function( titulo ) {
  
    const tituloArray = this.filter( serie => serie.titulo.includes( titulo ));
    const titulosDasSeries = tituloArray.map( element => element.titulo )
  
    return titulosDasSeries;
  }
  
  console.log(`Exercicio 6 B - ${ series.queroTitulo( 'The' ) }` );
  
 /** Exercicio-7 */
  
  const ordenarPeloUltimo = arrayNomes => {
    let ultimoNome = [];
    let primeiroNome = [];
    let nomesCompletos = [];
    
    for (let index = 0; index < arrayNomes.length; index++) {
      let element = arrayNomes[index].split( ' ' );
      ultimoNome.push( element[ element.length - 1 ] );
      primeiroNome.push( element[0]);
    }
  
    ultimoNome.sort( ( a, b ) => a > b ? 1 : -1 ) ;
    primeiroNome.sort( ( a, b ) => a > b ? 1 : -1 ) ;
  
    for (let i = 0; i < ultimoNome.length; i++) {
      for (let j = 0; j < arrayNomes.length; j++) {
        if( arrayNomes[j].includes( ultimoNome[i] ) && arrayNomes[j].includes( primeiroNome[i] ) ) {
          nomesCompletos.push( arrayNomes[j] );
        }
        
      }
    }
  
    return nomesCompletos;
  }
  
  Array.prototype.creditos = function(serie) {
    let titulo = `${ serie.titulo }\n`;
    let diretores = `Diretores:`;
    let atores = `\nElenco:`;
  
    let direcao = ordenarPeloUltimo(serie.diretor);
    let artistas = ordenarPeloUltimo(serie.elenco);
  
    for (let index = 0; index < direcao.length; index++) {
      diretores = `${ diretores }\n ${ direcao[index] }`
    }
  
    for (let index = 0; index < artistas.length; index++) {
      atores = `${ atores } \n ${ artistas[index] }`
    }
  
    const creditoSeries = titulo + diretores + atores
  
    return creditoSeries;
  }
  
  console.log(`Exercicio 7 - ${ series.creditos(series[0]) }` );
  
  /** Exercicio-8 */
  
  const nomesAbreviados = elenco => {
    let contador = 0;
    for (let index = 0; index < elenco.length; index++) {
      if( elenco[index].match(/[A-Z]{1}\./g) ) {
        contador++;
      }
    }
    return contador > 0 && contador === elenco.length;
  }
  
  const criaHashTag = elenco => {
    let stringHash = '#';
    for (let index = 0; index < elenco.length; index++) {
      stringHash = stringHash + elenco[index].match( /[A-Z]{1}\./g )[0];
    }
    const palavra = stringHash.replace(/\./g, '');
    return palavra;
  }
  
  Array.prototype.identificaElencoNomesAbreviados = function() {
    const serie = this.filter( serie => nomesAbreviados ( serie.elenco ) );
    const palavraHash = criaHashTag(serie[0].elenco);
    return palavraHash;
  }
  
  console.log(`Exercicio 8 - ${ series.identificaElencoNomesAbreviados() }` );
