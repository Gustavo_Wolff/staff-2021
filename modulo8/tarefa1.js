/** EXERCICIO - 1 */

let circulo = {
    raio: 5,
    tipo: "C"
}

function calcularCirculo({ raio, tipo }) {
    if (tipo == "A") {
        return resultado = 2 * Math.PI * raio;
    } else if (tipo == "C") {
        return resultado = Math.PI * Math.pow(raio, 2);
    }
}

console.log(calcularCirculo(circulo));

/** EXERCICIO - 2 */

function naoBissexto(ano) {
    if ((ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0))) {
        return false;
    } else {
        return true;
    }
}

console.log(naoBissexto(2016));

/** EXERCICIO - 3 */

function somarPares(array) {
    let soma = 0;
    for (let i = 0; i < array.length; i++) {
        if (i % 2 == 0) {
            soma += array[i];
        }
    }
    return soma;
}

console.log(somarPares([1, 56, 4.34, 6, -2]))

/** EXERCICIO - 4 */

function adicionar(num1) {
    return function (num2) {
        return num1 + num2;
    }
}

console.log(adicionar(2)(3))

/** EXERCICIO - 5 */

function imprimirBRL(valor) {
    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();
    valor = "" + valor;
    c = valor.indexOf(".", 0);

    if (c > 0) {
        inteiro = valor.substring(0, c);
        decimal = valor.substring(c + 1, valor.length);
    } else {
        inteiro = valor;
    }

    for (j = inteiro.length, c = 0; j > 0; j -= 3, c++) {
        aux[c] = inteiro.substring(j - 3, j);
    }

    inteiro = "";
    for (c = aux.length - 1; c >= 0; c--) {
        inteiro += aux[c] + '.';
    }

    inteiro = inteiro.substring(0, inteiro.length - 1);

    decimal = parseInt(decimal);
    if (isNaN(decimal)) {
        decimal = "00";
    } else {
        decimal = "" + decimal;
        if (decimal.length === 1) {
            decimal = "0" + decimal;
        }
    }

    valor = "R$ " + inteiro + "," + decimal;

    return valor;

}

console.log(imprimirBRL(12.05))