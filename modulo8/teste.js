function cardapioIFood( veggie = true, comLactose = false ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
    //var i = cardapio.length
  
    if ( !comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }
    cardapio = [...cardapio, 'pastel de carne', 'empanada de legumes mabijosa'];
  
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
     cardapio.splice( cardapio.indexOf('enroladinho de salsicha'), 1 )     
     cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }
    
    let resultadoFinal = []
    let i = 0
    while (i < cardapio.length) {
      resultadoFinal.push(cardapio[i].toUpperCase()) 
      i++
    }
    return console.log(resultadoFinal);
  }
  
  cardapioIFood() // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]