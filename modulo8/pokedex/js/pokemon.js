class Pokemon {
  constructor(objDaApi) {
    this._id = objDaApi.id;
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._peso = objDaApi.weight;
    this._tipos = objDaApi.types;
    this._estatisticas = objDaApi.stats;

  }

  get id() {
    return `${this._id}`;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${this._altura * 10} cm`;
  }

  get peso() {
    return `${this._peso / 10} kg`;
  }

  get tipos() {
    return this._tipos;
  }

  get estatisticas(){
    return this._estatisticas;
  }

  listarTipos() {
    const pai = document.getElementById( 'tipos' );
    const filho = pai.querySelector( 'ul' );
    const filhoSpan = pai.querySelector( 'span' );

    if ( filho !== null ) {
      filho.parentNode.removeChild( filho );
      filhoSpan.parentNode.removeChild( filhoSpan );
    }
    const div = document.getElementById( 'tipos' );
    const span = document.createElement( 'span' );
    span.innerHTML = 'Tipos';
    div.appendChild( span );
    const ul = document.createElement( 'ul' );
    div.appendChild( ul );
    for ( let i = 0; i < this.tipos.length; i += 1 ) {
      const li = document.createElement( 'li' );
      li.innerHTML = this.tipos[i].type.name;
      ul.appendChild( li );
    }
  }

  listarEstatisticas() {
    const pai = document.getElementById( 'estatisticas' );
    const filho = pai.querySelector( 'ul' );
    const filhoSpan = pai.querySelector( 'span' );

    if ( filho !== null ) {
      filho.parentNode.removeChild( filho );
      filhoSpan.parentNode.removeChild( filhoSpan );
    }
    const div = document.getElementById( 'estatisticas' );
    const span = document.createElement( 'span' );
    span.innerHTML = 'Estatisticas';
    div.appendChild( span );
    const ul = document.createElement( 'ul' );
    div.appendChild( ul );
    for ( let i = 1; i < this.estatisticas.length; i += 1 ) {
      const li = document.createElement( 'li' );
      li.innerHTML = `${ this.estatisticas[i].stat.name } ${ this.estatisticas[i].base_stat }`;
      ul.appendChild( li );
    }
  }


}