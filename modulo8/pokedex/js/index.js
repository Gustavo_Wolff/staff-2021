/*let dadosPokemon = document.getElementById("dadosPokemon");

let pokeApi = new PokeApi();
let buscou = pokeApi.buscarEspecifico(112);
buscou.then( pokemon => {
  let nome = dadosPokemon.querySelector(".nome");
  nome.innerHTML = pokemon.name;

  let imagem = dadosPokemon.querySelector(".thumb");
  imagem.src = pokemon.sprites.front_default;
});*/

let pokeApi = new PokeApi();
pokeApi.buscar(25)
  .then( pokemon => {
    let poke = new Pokemon( pokemon );
    renderizar( poke );
});

renderizar = ( pokemon ) => {
  let dadosPokemon = document.getElementById("dadosPokemon");

  let nome = dadosPokemon.querySelector(".nome");
  nome.innerHTML = pokemon.nome;

  let imagem = dadosPokemon.querySelector(".thumb");
  imagem.src = pokemon.imagem;

  let altura = dadosPokemon.querySelector(".altura");
  altura.innerHTML = pokemon.altura;

  let peso = dadosPokemon.querySelector(".peso");
  peso.innerHTML = pokemon.peso;

  let id = dadosPokemon.querySelector(".id");
  id.innerHTML = pokemon.id;

  pokemon.listarTipos();

  pokemon.listarEstatisticas();

}



const numeros = document.getElementsByClassName( 'numeros' );
const busca = document.getElementById( 'inputDisplay' );
const botaoBuscar = document.getElementById( 'enviar' );
const sorteado = [];
const proximo = document.getElementById( 'proximo' );
const anterior = document.getElementById( 'anterior' );
const sortear = document.getElementById( 'sorteador' );
let numeroPokemon = '';



busca.addEventListener( 'blur', () => {
  numeroPokemon = busca.value;
  if ( sorteado[sorteado.length - 1] !== numeroPokemon ) {
    buscar( numeroPokemon );
    sorteado.push( numeroPokemon );
  }
} );

botaoBuscar.addEventListener( 'click', () => {
  numeroPokemon = busca.value;
  if ( sorteado[sorteado.length - 1] !== numeroPokemon ) {
    buscar( numeroPokemon );
    sorteado.push( numeroPokemon );
  }
} );

// avancar para o proximo Pokemon

proximo.addEventListener( 'click', ( event ) => {
  if ( event.screenX !== 0 ) {
    numeroPokemon = parseInt( numeroPokemon, 10 ) + 1;
    buscar( numeroPokemon );
  }
} );

// retornar para o pokemon anterior

anterior.addEventListener( 'click', ( event ) => {
  if ( event.screenX !== 0 ) {
    numeroPokemon = parseInt( numeroPokemon, 10 ) - 1;
    this.buscar( numeroPokemon );
  }
} );

sortear.addEventListener( 'click', ( event ) => {
  let i = 1;
  while ( i > 0 ) {
    numeroPokemon = Math.floor( Math.random() * 802 + 1 );
    i = 0;
    for ( let j = 0; j < sorteado.length; j += 1 ) {
      if ( sorteado[j] === numeroPokemon ) {
        i += 1;
      }
    }
  }
  if ( event.screenX !== 0 ) {
    this.buscar( numeroPokemon );
    sorteado.push( numeroPokemon );
  }
} );

function findPoke() {
  pokeApi.buscarPokemon(pos)
    .then(
      pokemon => {
        render.renderizar(new Pokemon(pokemon));
        pos = pokemon.id;
        document.getElementById("input_name").placeholder = "Nome ou ID do pokemon";

        window.speechSynthesis.cancel();
        let msg = new SpeechSynthesisUtterance(pokemon.name);
        window.speechSynthesis.speak(msg);

      }).catch(function (_err) {
        pos = 1;
        document.getElementById("input_name").value = "";
        document.getElementById("input_name").placeholder = "Digite um id válido!";
      });
}

function searchPoke() {
  if (pos == document.getElementById("input_name").value) {
    return null;
  }
  pos = document.getElementById("input_name").value;
  findPoke();
}


