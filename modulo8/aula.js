console.log("Chegou até aqui!");

/** VAR */
console.log(nome);
var nome = "Marcos";
var nome = "Marcos H";
console.log(nome);

/** LET */

let nome1 = "Marcos";

    {
        let nome1 = "Marcos";
        nome1 = "Marcos H";
        console.log(nome1);
    }

console.log(nome1);

/** CONST */

//const nome2 = "Marcos - Constante";
const pessoa = {
    nome: "Marcos - Constante"
};

Object.freeze(pessoa);

pessoa.nome= "Marcos -Constante 2";
//pessoa.idade = 31;

console.log(pessoa.nome);
//console.log(pessoa.idade);

/** ESPETACULAR */
console.log("Nossa que incrível!!!!!");

let soma = 1 + 2;
let soma1 = "1" + 2 + 3;
let soma2 = 1 + "2" + 3;
let soma3 = 1 + 2 + "3";

console.log(soma);
console.log(soma1);
console.log(soma2);
console.log(soma3);

/** FUNCOES!!! */

let nomeFuncao = "Marcos";
let idadeFuncao = 31
let semestre = 5;
let notas = [10.0, 3, 5, 9];

function funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas = []) {
    const aluno = {
        nome: nomeFuncao,
        idade: idadeFuncao,
        semestre: semestre,
        nota: notas
    };

    //Factory -> Design Pattern
    function aprovadoOrReprovado( notas ) {
        if(notas.length == 0){
            return "Sem Notas";
        }

        let somatoria = 0;
        for(let i = 0; i < notas.length; i++) {
            somatoria += notas[i];
        }

        return (somatoria/ notas.length) > 7.0 ? "Aprovado" : "Reprovado";
    }

    aluno.status = aprovadoOrReprovado(notas);
    console.log(aluno);
    return aluno;

    console.log(aluno);
    return aluno;
}


funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas);
funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre);

/*let alunoExterno = funcaoCriarAluno();
alunoExterno.nota = 11;

console.log(alunoExterno);*/


let i = 2;

i==2
i == "2"



