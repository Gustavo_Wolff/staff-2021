import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import './botao.css'

const BotaoUi = ( { cor, metodo, nome, link } ) =>
  <React.Fragment>
    <Button color={cor} onClick={ metodo }>
      { link ? <Link to={ link } className='meu-link'>{ nome }</Link> : nome }
    </Button>
  </React.Fragment>

BotaoUi.propTypes = {
  nome: PropTypes.string.isRequired,
  metodo:PropTypes.func,
  classe: PropTypes.string
}

export default BotaoUi;