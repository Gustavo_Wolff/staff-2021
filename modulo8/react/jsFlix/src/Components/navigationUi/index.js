import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../assets/img/logo.png';
import BotaoUi from '../botaoUi';

import './navigationUi.css';
import { EpisodiosApi, ListaEpisodios } from '../../models';

export default class NavigationUi extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.buscarTodosDetalhes()
    ]

    Promise.all( requisicoes )
    .then( respostas => {
      const episodiosDoServidor = respostas[ 0 ];
      const notasServidor = respostas[ 1 ];
      const detalhesDoSeridor = respostas[ 2 ];
      const listaEpisodiosNova = new ListaEpisodios( episodiosDoServidor, notasServidor, detalhesDoSeridor );
      const episodio = listaEpisodiosNova.episodiosAleatorios;
      const id = episodio.id;
      this.setState( state => { return { ...state, id, listaEpisodios: listaEpisodiosNova._todos } } );
    } )
  }

  reload = () => {
    
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.buscarTodosDetalhes()
    ]

    Promise.all( requisicoes )
    .then( respostas => {
      const episodiosDoServidor = respostas[ 0 ];
      const notasServidor = respostas[ 1 ];
      const detalhesDoSeridor = respostas[ 2 ];
      const listaEpisodiosNova = new ListaEpisodios( episodiosDoServidor, notasServidor, detalhesDoSeridor );
      const episodio = listaEpisodiosNova.episodiosAleatorios;
      const id = episodio.id;
      this.setState( state => { return { ...state, id, listaEpisodios: listaEpisodiosNova._todos } } );
    } )

    window.location.reload();
  }

  handler = ( e ) => {
    e.preventDefault();
    localStorage.removeItem('user')
  }

  render() {
    const { id } = this.state;
    const { metodo } = this.props;
    return (
    <div className='nav-bar-wrapper'>
      <div className='nav-bar'>
        <ul>
          <li>
            <Link to='/'><img className='logo' src={Logo} alt='Logo PreguiçaFlix' /></Link>
          </li>
          <li>
            <BotaoUi cor='primary' nome='Ranking' link='/ranking'/>
          </li>
          <li>
            <BotaoUi cor='secondary' 
                      nome='Assistir aleatório' 
                      link={ `/episodio/${ id }` } metodo={ this.reload } />
          </li>
          <li>
            <Link to='/'><input type='text' placeholder='Buscar...' onKeyPress={ metodo }/></Link>
          </li>
          <li>
            <BotaoUi cor='primary' nome='logout' link='/login' metodo={ this.handler }/>
          </li>
        </ul>
      </div>
    </div>
    )
  }
}