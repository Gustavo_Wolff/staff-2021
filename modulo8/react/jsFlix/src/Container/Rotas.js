import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import App from './App';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/teste" component={ PaginaTeste } />
      </Router>
    );
  }
}

const PaginaTeste = () =>
  <div>
    <h1>Página Teste</h1>
    <Link to="/">Página App</Link>
  </div>