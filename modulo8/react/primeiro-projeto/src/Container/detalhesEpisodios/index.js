import React, { Component } from 'react';
import { EpisodioAPI, Episodio } from '../../models';
import EpisodioUi from '../../components/EpisodioUi';

export default class DetalhesEpisodios extends Component {
  constructor( props ) {
    super( props );
    this.EpisodioAPI = new EpisodioAPI();
    this.state = {
      detalhes: null
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.EpisodioAPI.buscarEpisodio( episodioId ),
      this.EpisodioAPI.buscarDetalhes( episodioId ),
      this.EpisodioAPI.buscarNota( episodioId )
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl } = respostas[0];
        this.setState({
          episodio: new Episodio(id, nome, duracao, temporada, ordemEpisodio, thumbUrl),
          detalhes: respostas[1],
          objNota: respostas[2]
        })
      })
  }

  render() {
    const { episodio, detalhes, objNota } = this.state;
    
    return(
      <React.Fragment>
        <header className="App-header">
          { episodio && ( <EpisodioUi episodio={ episodio } /> )}
          {
            detalhes ?
            <React.Fragment>
              <p>{ detalhes.sinopse }</p>
              <span>{ new Date( detalhes.dataEstreia ).toLocaleDateString() }</span>
              <span>IMDb: { detalhes.notaImdb * 0.5 }</span>
              <span>Sua nota: { objNota ? objNota.nota : 'N/D' }</span>
            </React.Fragment> : null
          }
        </header>
      </React.Fragment>
    )
  }
}