package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Repository.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContratacaoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @BeforeEach
    public void gerarDadosDeEntrada() {
        //Cliente
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newTipoContato);
        contato.setValor("fulano@fulano.com");
        ContatoEntity c1 = contatoRepository.save(contato);

        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("5151515151");
        ContatoEntity c2 = contatoRepository.save(contato2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("novoFulano");
        cliente.setCpf("01987654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);
        ClienteEntity clienteEntity = clienteRepository.save(cliente);
        int idCliente = clienteEntity.getId();

        //Espaco
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 2");
        espaco.setQtdPessoas(30);
        espaco.setValor(300.00);
        EspacoEntity newEspaco = espacoRepository.save(espaco);
        int idEspaco = newEspaco.getId();

    }

    @AfterEach
    public void limpar() {
        contratacaoRepository.deleteAll();
        espacoRepository.deleteAll();
        clienteRepository.deleteAll();
        contatoRepository.deleteAll();
        tipoContatoRepository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoConsultadoContratacaoEntity () throws  Exception {
        URI uri = new URI("/api/contratacao/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmContratacaoEntity() throws Exception {

        //Cliente
        ClienteEntity cliente = clienteRepository.findByCpf("01987654321");
        int idCliente = cliente.getId();

        //Espaco
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");
        int idEspaco = espaco.getId();

        URI uri = new URI("/api/contratacao/novo");
        String json = "{\"cliente\": {\"id\": " + idCliente + "}," +
                "\"espaco\": {\"id\": " + idEspaco + "}," +
                "\"quantidade\":" + 2 + "," +
                "\"prazo\": 2," +
                "\"tipoContratacao\": \"MES\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao").value("MES")
        );
    }

    @Test
    public void deveRetornarUmContratacaoEntity() throws Exception {

        ClienteEntity cliente = clienteRepository.findByCpf("01987654321");
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(cliente);
        contratacao.setEspaco(espaco);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        contratacao.setQuantidade(5);
        contratacao.setPrazo(3);
        ContratacaoEntity newContratacao= contratacaoRepository.save(contratacao);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/contratacao/ver/{id}", newContratacao.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao")
                .value("DIARIA")
        );
    }

    @Test
    public void deveEditarERetornarUmContratacao() throws Exception {

        ClienteEntity cliente = clienteRepository.findByCpf("01987654321");
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(cliente);
        contratacao.setEspaco(espaco);
        contratacao.setTipoContratacao(TipoContratacaoEnum.SEMANA);
        contratacao.setQuantidade(5);
        contratacao.setPrazo(2);
        ContratacaoEntity newContratacao= contratacaoRepository.save(contratacao);

        int idEspaco = espaco.getId();
        int idCliente = cliente.getId();

        String json = "{\"quantidade\": 5," +
                "\"espaco\": {\"id\":" + idEspaco + "}," +
                "\"cliente\": {\"id\":" + idCliente + "}," +
                "\"prazo\": 2," +
                "\"tipoContratacao\": \"MES\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/contratacao/editar/{id}", newContratacao.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao")
                .value("MES")
        );
    }

}

