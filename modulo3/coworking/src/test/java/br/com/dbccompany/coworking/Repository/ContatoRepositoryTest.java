package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    ContatoRepository repository;

    @Autowired
    TipoContatoRepository tipoContatoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Test
    public void salvaContato() {

        ContatoEntity contato = new ContatoEntity();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Email");
        tipoContatoRepository.save(tipoContato);
        List<TipoContatoEntity> tiposContatosEntity = (List<TipoContatoEntity>) tipoContatoRepository.findAll();

        contato.setTipoContato(tiposContatosEntity.get(0));
        contato.setValor("chinchila@gov.br");

        repository.save(contato);

        assertEquals("chinchila@gov.br", repository.findByValor("chinchila@gov.br").getValor());
    }

    @Test
    public void retornaOptionalEmptySeTipoContatoForCriado() {
        String nome = "chinchila@gov.br";
        assertEquals(Optional.empty(), repository.findById(1));
    }

    @Test
    public void salvaDoisContatosERetornaPorTipoContato() {

        ContatoEntity contato = new ContatoEntity();
        ContatoEntity contatoEntity = new ContatoEntity();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Email");
        tipoContatoRepository.save(tipoContato);
        List<TipoContatoEntity> tiposContatosEntity = (List<TipoContatoEntity>) tipoContatoRepository.findAll();

        contato.setTipoContato(tiposContatosEntity.get(0));
        contato.setValor("doninha@gov.br");
        contatoEntity.setTipoContato(tiposContatosEntity.get(0));
        contatoEntity.setValor("texugo@gov.br");

        repository.save(contato);
        repository.save(contatoEntity);

        assertEquals(2, repository.findByTipoContato(tipoContato).size());
        assertEquals("doninha@gov.br", repository.findByTipoContato(tipoContato).get(0).getValor());
        assertEquals("texugo@gov.br", repository.findByTipoContato(tipoContato).get(1).getValor());
    }

    @Test
    public void salvaDoisContatosERetornaPorClienteEClienteTipoContato() {
        ContatoEntity contato = new ContatoEntity();
        ContatoEntity contatoEntity = new ContatoEntity();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Email");
        tipoContatoRepository.save(tipoContato);
        List<TipoContatoEntity> tiposContatosEntity = (List<TipoContatoEntity>) tipoContatoRepository.findAll();

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito montoia de la mancha");
        cliente.setDataNascimento(new Date(1977, 7, 17));
        cliente.setCpf("12345678910");
        clienteRepository.save(cliente);
        ClienteEntity newCliente = clienteRepository.findByCpf("12345678910");

        contato.setTipoContato(tiposContatosEntity.get(0));
        contato.setValor("doninha@gov.br");
        contato.setCliente(newCliente);
        contatoEntity.setTipoContato(tiposContatosEntity.get(0));
        contatoEntity.setValor("doninha@gov.br");

        repository.save(contato);
        repository.save(contatoEntity);


        assertEquals("doninha@gov.br", repository.findByCliente(newCliente).get(0).getValor());
        assertEquals("doninha@gov.br", repository.findByTipoContatoAndCliente(tiposContatosEntity.get(0), newCliente).get(0).getValor() );

    }
}

