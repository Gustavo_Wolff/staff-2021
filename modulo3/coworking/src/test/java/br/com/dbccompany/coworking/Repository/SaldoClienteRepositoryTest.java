package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class SaldoClienteRepositoryTest {

    @Autowired
    SaldoClienteRepository repository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Test
    public void salvaSaldoCliente() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("infierno");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        clienteRepository.save(cliente);

        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(clienteRepository.findByCpf("12345678910").getId());
        saldoClienteId.setIdEspaco(espacoRepository.findByNome("infierno").getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setQuantidade(30);
        LocalDate data = LocalDate.now();
        saldoCliente.setVencimento(data);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        repository.save(saldoCliente);

        List<SaldoClienteEntity> saldoClientes = (List<SaldoClienteEntity>) repository.findAll();

        assertEquals(30, repository.findByTipoContratacao(TipoContratacaoEnum.DIARIA).get(0).getQuantidade());
        assertEquals(30, repository.findByVencimento(data).get(0).getQuantidade());
        assertEquals(data.getDayOfMonth(), repository.findByQuantidade(30).get(0).getVencimento().getDayOfMonth());
    }

    @Test
    public void retornaOptionalEmptySeSaldoCliente() {

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("infierno");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        clienteRepository.save(cliente);

        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(clienteRepository.findByCpf("12345678910").getId());
        saldoClienteId.setIdEspaco(espacoRepository.findByNome("infierno").getId());

        assertEquals(Optional.empty(),repository.findById(saldoClienteId));
    }

    @Test
    public void salvaDoisSaldoClienteERetornaTodos() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("infierno");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("infiernoDos");
        espaco2.setQtdPessoas(30);
        espaco2.setValor(800.00);
        espacoRepository.save(espaco2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        clienteRepository.save(cliente);

        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(clienteRepository.findByCpf("12345678910").getId());
        saldoClienteId.setIdEspaco(espacoRepository.findByNome("infierno").getId());

        SaldoClienteId saldoClienteId2 = new SaldoClienteId();
        saldoClienteId2.setIdCliente(clienteRepository.findByCpf("12345678910").getId());
        saldoClienteId2.setIdEspaco(espacoRepository.findByNome("infiernoDos").getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setQuantidade(30);
        LocalDate data = LocalDate.now();
        saldoCliente.setVencimento(data);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        repository.save(saldoCliente);

        SaldoClienteEntity saldoCliente2 = new SaldoClienteEntity();
        saldoCliente2.setId(saldoClienteId2);
        saldoCliente2.setQuantidade(30);
        LocalDate data2 = LocalDate.now();
        saldoCliente2.setVencimento(data2);
        saldoCliente2.setTipoContratacao(TipoContratacaoEnum.MES);
        repository.save(saldoCliente2);

        List<SaldoClienteEntity> saldoClientes = (List<SaldoClienteEntity>) repository.findAll();

        assertEquals(TipoContratacaoEnum.DIARIA, saldoClientes.get(0).getTipoContratacao());
        assertEquals(TipoContratacaoEnum.MES, saldoClientes.get(1).getTipoContratacao());
    }

}

