package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @BeforeEach
    public void gerarDadosDeEntrada() {
        //Cliente
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newTipoContato);
        contato.setValor("fulano@fulano.com");
        ContatoEntity c1 = contatoRepository.save(contato);

        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("5151515151");
        ContatoEntity c2 = contatoRepository.save(contato2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("novoFulano");
        cliente.setCpf("01987654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);
        ClienteEntity clienteEntity = clienteRepository.save(cliente);
        int idCliente = clienteEntity.getId();

    }

    @AfterEach
    public void limpar() {
        clienteRepository.deleteAll();
        contatoRepository.deleteAll();
        tipoContatoRepository.deleteAll();
    }


    @Test
    public void deveRetornar200QuandoConsultadoClienteEntity () throws  Exception {
        URI uri = new URI("/api/cliente/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmClienteEntity() throws Exception {
        int idContato = contatoRepository.findByValor("fulano@fulano.com").getId();
        int idContato2 = contatoRepository.findByValor("5151515151").getId();

        URI uri = new URI("/api/cliente/novo");
        String json = "{\"nome\": \"fulano\"," +
                "\"cpf\": \"12345678910\"," +
                "\"dataNascimento\": \"1985-02-25\"," +
                "\"contatos\": [{\"id\":" + idContato + "}," +
                "{\"id\":" + idContato2 + "}]}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("fulano")
        );
    }

    @Test
    public void deveRetornarUmClienteEntity() throws Exception {

        ContatoEntity c1 = contatoRepository.findByValor("fulano@fulano.com");
        ContatoEntity c2 = contatoRepository.findByValor("5151515151");

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("novoFulano");
        cliente.setCpf("91987654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);

        ClienteEntity newCliente = clienteRepository.save(cliente);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/cliente/ver/{id}", newCliente.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("novoFulano")
        );
    }

    @Test
    public void deveEditarERetornarUmCliente() throws Exception {
        ContatoEntity c1 = contatoRepository.findByValor("fulano@fulano.com");
        ContatoEntity c2 = contatoRepository.findByValor("5151515151");

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("semNome");
        cliente.setCpf("00087654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);

        ClienteEntity newCliente = clienteRepository.save(cliente);

        int idContato = c1.getId();
        int idContato2 = c2.getId();

        String json = "{\"nome\": \"ciclano\"," +
                "\"cpf\": \"00087654321\"," +
                "\"dataNascimento\": \"1985-02-25\"," +
                "\"contatos\": [{\"id\":" + idContato + "}," +
                "{\"id\":" + idContato2 + "}]}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/cliente/editar/{id}", newCliente.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("ciclano")
        );
    }
}

