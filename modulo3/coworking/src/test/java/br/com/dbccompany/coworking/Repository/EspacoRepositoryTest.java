package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EspacoRepositoryTest {

    @Autowired
    EspacoRepository repository;

    @Test
    public void salvaEspacoERecuparaPorQtdPessoasNomeEValor() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("infierno");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        repository.save(espaco);

        assertEquals(800.00, repository.findByNome("infierno").getValor(), 0.00001);
        assertEquals(30, repository.findByValor(800.00).get(0).getQtdPessoas());
    }

    @Test
    public void retornaOptionalEmptySePacoteForCriado() {
        double valor = 800.00;
        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisPacotesERetornaTodos() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("infierno");
        espaco.setQtdPessoas(30);
        espaco.setValor(500.00);
        repository.save(espaco);

        EspacoEntity espacoNovo = new EspacoEntity();
        espacoNovo.setNome("infiernoDos");
        espacoNovo.setQtdPessoas(30);
        espacoNovo.setValor(600.00);
        repository.save(espacoNovo);

        List<EspacoEntity> espacos = (List<EspacoEntity>) repository.findAll();

        assertEquals("infierno", espacos.get(0).getNome());
        assertEquals("infiernoDos", espacos.get(1).getNome());
    }
}

