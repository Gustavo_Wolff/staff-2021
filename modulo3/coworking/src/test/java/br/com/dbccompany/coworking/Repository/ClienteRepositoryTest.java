package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ClienteRepositoryTest {
    @Autowired
    ClienteRepository repository;

    @Test
    public void salvaClienteERetornaPorDataNscimentoNomeECpf() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        repository.save(cliente);

        assertEquals("12345678910", repository.findByNome("juanito").get(0).getCpf());
        assertEquals("juanito", repository.findByDataNascimento(new Date(1977, 3, 12)).get(0).getNome());
    }

    @Test
    public void retornaOptionalEmptySePacoteForCriado() {
        String nome = "juanito";
        String cpf = "12345678910";
        Date dataNascimentto = new Date(1977, 3, 12);
        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisClientesERetornaTodos() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        repository.save(cliente);

        ClienteEntity clienteNovo = new ClienteEntity();
        clienteNovo.setNome("elpatron");
        clienteNovo.setCpf("10987654321");
        clienteNovo.setDataNascimento(new Date(1977, 5, 12));
        repository.save(clienteNovo);

        List<ClienteEntity> clientes = (List<ClienteEntity>) repository.findAll();

        assertEquals("12345678910", clientes.get(0).getCpf());
        assertEquals("10987654321", clientes.get(1).getCpf());
    }
}

