package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository repository;

    @Test
    public void salvaUsuarioERetornaPorLogin() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Juanito Montoia de la Mancha");
        usuario.setEmail("juan@mexico.com");
        usuario.setLogin("juanmontoia");
        usuario.setSenha("adminadmin");
        repository.save(usuario);
        assertEquals(usuario.getNome(), repository.findByLogin("juanmontoia").getNome());
        assertEquals(usuario.getEmail(), repository.findByLogin("juanmontoia").getEmail());
        assertEquals(usuario.getLogin(), repository.findByLogin("juanmontoia").getLogin());
        assertEquals(usuario.getSenha(), repository.findByLogin("juanmontoia").getSenha());
    }

    @Test
    public void salvaUsuarioERetornaPorLoginESenha() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Juanito Montoia de la Mancha");
        usuario.setEmail("juan@mexico.com");
        usuario.setLogin("juanmontoia");
        usuario.setSenha("adminadmin");
        repository.save(usuario);
        assertEquals(usuario.getNome(), repository.findByLoginAndSenha("juanmontoia","adminadmin").getNome());
        assertEquals(usuario.getEmail(), repository.findByLoginAndSenha("juanmontoia", "adminadmin").getEmail());
        assertEquals(usuario.getLogin(), repository.findByLoginAndSenha("juanmontoia", "adminadmin").getLogin());
        assertEquals(usuario.getSenha(), repository.findByLoginAndSenha("juanmontoia", "adminadmin").getSenha());
    }

    @Test
    public void retornaNullSeUsuarioNaoForCriado() {
        String login = "juanmontoia";
        String senha = "adminadmin";
        assertNull(repository.findByLogin(login));
        assertNull(repository.findByLoginAndSenha(login, senha));
    }

    @Test
    public void salvaDoisUsuariosERetornaTodosPorLogin() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("Juanito Montoia de la Mancha");
        usuario.setEmail("juan@mexico.com");
        usuario.setLogin("juanmontoia");
        usuario.setSenha("adminadmin");
        repository.save(usuario);

        UsuarioEntity usuarioEntity = new UsuarioEntity();
        usuarioEntity.setNome("Pablo Escovinha");
        usuarioEntity.setEmail("pabloescovinha@mexico.com");
        usuarioEntity.setLogin("pabloescovinha");
        usuarioEntity.setSenha("123456");
        repository.save(usuarioEntity);

        List<UsuarioEntity> usuarios = (List<UsuarioEntity>) repository.findAll();

        assertEquals("juanmontoia", repository.findByLogin("juanmontoia").getLogin());
        assertEquals("pabloescovinha", repository.findByLogin("pabloescovinha").getLogin());
    }

}

