package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ClientePacoteRepositoryTest {

    @Autowired
    ClientePacoteRepository repository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Test
    public void salvaClentePacoteEReotornaPorClientePacoteQuantidade() {

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacotes.get(0));
        clientePacote.setCliente(clienteRepository.findByCpf("12345678910"));
        clientePacote.setQuantidade(30);
        repository.save(clientePacote);

        List<ClientePacoteEntity> clientePacotes = (List<ClientePacoteEntity>) repository.findAll();

        assertEquals(30, repository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0).getQuantidade());
        assertEquals(30, repository.findByPacote(pacotes.get(0)).get(0).getQuantidade());
        assertEquals("12345678910", repository.findByQuantidade(30).get(0).getCliente().getCpf());
    }

    @Test
    public void retornaOptionalEmptySeClientePacoteForCriado() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);

        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisClientesPacotesERetornaTodos() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("juanito");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1977, 3, 12));
        clienteRepository.save(cliente);

        ClienteEntity cliente2 = new ClienteEntity();
        cliente2.setNome("elpatron");
        cliente2.setCpf("10987654321");
        cliente2.setDataNascimento(new Date(1977, 5, 12));
        clienteRepository.save(cliente2);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacotes.get(0));
        clientePacote.setCliente(clienteRepository.findByCpf("12345678910"));
        clientePacote.setQuantidade(30);
        repository.save(clientePacote);

        ClientePacoteEntity clientePacote2 = new ClientePacoteEntity();
        clientePacote2.setPacote(pacotes.get(0));
        clientePacote2.setCliente(clienteRepository.findByCpf("10987654321"));
        clientePacote2.setQuantidade(30);
        repository.save(clientePacote2);

        List<ClientePacoteEntity> clientesPacotes = (List<ClientePacoteEntity>) repository.findAll();

        assertEquals("12345678910", clientesPacotes.get(0).getCliente().getCpf());
        assertEquals("10987654321", clientesPacotes.get(1).getCliente().getCpf());
    }

}

