package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class PacoteRepositoryTest {

    @Autowired
    PacoteRepository repository;

    @Test
    public void salvaPacote() {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        repository.save(pacote);

        List<PacoteEntity> pacotes = (List<PacoteEntity>) repository.findAll();

        assertEquals(pacote.getValor(), pacotes.get(0).getValor(), 0.00001);
    }

    @Test
    public void retornaOptionalEmptySePacoteForCriado() {
        double valor = 800.00;
        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisPacotesERetornaTodos() {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(500.00);
        repository.save(pacote);

        PacoteEntity pacoteNovo = new PacoteEntity();
        pacoteNovo.setValor(600.00);
        repository.save(pacoteNovo);

        List<PacoteEntity> pacotes = (List<PacoteEntity>) repository.findAll();

        assertEquals(500.00, pacotes.get(0).getValor(), 0.00001);
        assertEquals(600.00, pacotes.get(1).getValor(), 0.00001);
    }
}

