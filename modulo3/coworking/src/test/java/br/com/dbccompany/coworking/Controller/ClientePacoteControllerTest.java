package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Repository.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClientePacoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    @BeforeEach
    public void gerarDadosDeEntrada() {
        //Cliente
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newTipoContato);
        contato.setValor("fulano@fulano.com");
        ContatoEntity c1 = contatoRepository.save(contato);

        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("5151515151");
        ContatoEntity c2 = contatoRepository.save(contato2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("novoFulano");
        cliente.setCpf("01987654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);
        ClienteEntity clienteEntity = clienteRepository.save(cliente);
        int idCliente = clienteEntity.getId();

        //Pacote
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200.00);
        PacoteEntity pacoteEntity = pacoteRepository.save(pacote);


    }

    @AfterEach
    public void limpar() {
        clientePacoteRepository.deleteAll();
        clienteRepository.deleteAll();
        contatoRepository.deleteAll();
        tipoContatoRepository.deleteAll();
        pacoteRepository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoConsultadoClientePacoteEntity () throws  Exception {
        URI uri = new URI("/api/clientePacote/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmClientePacoteEntity() throws Exception {
        int idCliente = clienteRepository.findByCpf("01987654321").getId();
        int idPacote = pacoteRepository.findByValor(200.00).get(0).getId();

        URI uri = new URI("/api/clientePacote/novo");
        String json = "{\"cliente\": {\"id\": " + idCliente + "}," +
                "\"pacote\": {\"id\": " + idPacote + "}," +
                "\"quantidade\":" + 2 + "}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(2)
        );
    }

    @Test
    public void deveRetornarUmClientePacoteEntity() throws Exception {

        ClienteEntity cliente = clienteRepository.findByCpf("01987654321");
        PacoteEntity pacote = pacoteRepository.findByValor(200.0).get(0);
        ClientePacoteEntity clientePacoteEntity = new ClientePacoteEntity();
        clientePacoteEntity.setCliente(cliente);
        clientePacoteEntity.setPacote(pacote);
        clientePacoteEntity.setQuantidade(5);
        ClientePacoteEntity newClientePacoteEntity= clientePacoteRepository.save(clientePacoteEntity);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/clientePacote/ver/{id}", newClientePacoteEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value(5)
        );
    }

    @Test
    public void deveEditarERetornarUmClientePacote() throws Exception {

        ClienteEntity cliente = clienteRepository.findByCpf("01987654321");
        PacoteEntity pacote = pacoteRepository.findByValor(200.0).get(0);
        ClientePacoteEntity clientePacoteEntity = new ClientePacoteEntity();
        clientePacoteEntity.setCliente(cliente);
        clientePacoteEntity.setPacote(pacote);
        clientePacoteEntity.setQuantidade(10);
        ClientePacoteEntity newClientePacoteEntity = clientePacoteRepository.save(clientePacoteEntity);

        int idCliente = cliente.getId();
        int idPacote = pacote.getId();

        String json = "{\"quantidade\": 12," +
                "\"cliente\": {\"id\":" + idCliente + "}," +
                "\"pacote\": {\"id\":" + idPacote + "}}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/clientePacote/editar/{id}", newClientePacoteEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value(12)
        );
    }
}

