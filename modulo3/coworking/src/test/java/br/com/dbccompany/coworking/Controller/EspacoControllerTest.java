package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EspacoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacoRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoEspacoEntity () throws  Exception {
        URI uri = new URI("/api/espaco/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmEspacoEntity() throws Exception {
        URI uri = new URI("/api/espaco/novo");
        String json = "{\"nome\": \"Area 1\"," +
                "\"qtdPessoas\": 30," +
                "\"valor\": \"R$ 2.000,00\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Area 1")
        );
    }

    @Test
    public void deveRetornarUmEspacoEntity() throws Exception {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 2");
        espaco.setQtdPessoas(30);
        espaco.setValor(300.00);

        EspacoEntity newEspaco = repository.save(espaco);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espaco/ver/{id}", newEspaco.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("Area 2")
        );
    }

    @Test
    public void deveEditarERetornarUmEspaco() throws Exception {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 3");
        espaco.setQtdPessoas(30);
        espaco.setValor(300.00);

        EspacoEntity newTipoContato = repository.save(espaco);
        String json = "{\"nome\": \"Area 4\"," +
                "\"qtdPessoas\": 30," +
                "\"valor\": \"R$ 300,00\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/espaco/editar/{id}", newTipoContato.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("Area 4")
        );
    }
}

