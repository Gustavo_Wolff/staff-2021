package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Repository.*;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PagamentoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Test
    @Order(1)
    public void deveRetornar200QuandoConsultadoPagamentoEntity () throws  Exception {
        URI uri = new URI("/api/pagamento/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    @Order(2)
    public void salvarRetornarUmPagamentoEntityComContratacao() throws Exception {
        //Precisa de Espaco e EspacoPacote para realizar as regras de negocio
        //Espaco
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 2");
        espaco.setQtdPessoas(30);
        espaco.setValor(300.00);
        EspacoEntity newEspaco = espacoRepository.save(espaco);

        //Cliente
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newTipoContato);
        contato.setValor("fulano@fulano.com");
        ContatoEntity c1 = contatoRepository.save(contato);

        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("5151515151");
        ContatoEntity c2 = contatoRepository.save(contato2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("novoFulano");
        cliente.setCpf("01987654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);
        ClienteEntity clienteEntity = clienteRepository.save(cliente);

        //Contratacao
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(clienteEntity);
        contratacao.setEspaco(newEspaco);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        contratacao.setQuantidade(5);
        contratacao.setPrazo(3);
        ContratacaoEntity newContratacao= contratacaoRepository.save(contratacao);

        URI uri = new URI("/api/pagamento/novo");
        String json = "{\"contratacao\": {\"id\": 1," +
                "\"cliente\": {\"id\": 1}," +
                "\"espaco\": {\"id\": 1}," +
                "\"tipoContratacao\": \"DIARIA\"," +
                "\"quantidade\": 5," +
                "\"prazo\": 3}}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.contratacao.tipoContratacao").value("DIARIA")
        );
    }

    @Test
    @Transactional
    @Order(3)
    public void salvarRetornarUmPagamentoEntityComClientePacote() throws Exception {
        ClienteEntity clienteEntity = clienteRepository.findByCpf("01987654321");

        //Pacote
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200.00);
        PacoteEntity newPacote = pacoteRepository.save(pacote);
        //EspacoPacote
        EspacoPacoteEntity espacoPacoteEntity = new EspacoPacoteEntity();
        espacoPacoteEntity.setEspaco(espacoRepository.findByNome("Area 2"));
        espacoPacoteEntity.setPacote(newPacote);
        espacoPacoteEntity.setQuantidade(5);
        espacoPacoteEntity.setTipoContratacao(TipoContratacaoEnum.MES);
        espacoPacoteEntity.setQuantidade(1);
        EspacoPacoteEntity newEspacoPacoteEntity= espacoPacoteRepository.save(espacoPacoteEntity);
        //Atualiza Pacote com o EspacoPacote
        PacoteEntity pacote2 = pacoteRepository.findById(1).get();
        List<EspacoPacoteEntity> espacosPacotes = new ArrayList<>();
        espacosPacotes.add(espacoPacoteRepository.findById(1).get());
        pacote2.setEspacosPacotes(espacosPacotes);
        PacoteEntity newPacote2 = pacoteRepository.save(pacote2);

        //ClientePacote
        ClientePacoteEntity clientePacoteEntity = new ClientePacoteEntity();
        clientePacoteEntity.setCliente(clienteEntity);
        clientePacoteEntity.setPacote(newPacote2);
        clientePacoteEntity.setQuantidade(5);
        ClientePacoteEntity newClientePacoteEntity= clientePacoteRepository.save(clientePacoteEntity);
        int idClientePacote = newClientePacoteEntity.getId();



        URI uri = new URI("/api/pagamento/novo");
        String json = "{\"clientePacote\": {\"id\": 1," +
                "\"cliente\": {\"id\":" + clienteEntity.getId() +"}," +
                "\"pacote\": {\"id\":"+ newPacote2.getId() +"}," +
                "\"quantidade\": 5}}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.clientePacote.id").value(1)
        );
    }

    @Test
    @Order(4)
    public void deveRetornarUmPagamentoEntity() throws Exception {
        PagamentoEntity pagamentoEntity = pagamentoRepository.findById(1).get();


        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/pagamento/ver/{id}", pagamentoEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.contratacao.tipoContratacao").value("DIARIA")
        );
    }

    @Test
    @Order(5)
    public void deveEditarERetornarUmPagamentoEntity() throws Exception {
        PagamentoEntity pagamentoEntity = pagamentoRepository.findById(1).get();

        String json = "{\"contratacao\": {\"id\": 1," +
                "\"cliente\": {\"id\": 1}," +
                "\"espaco\": {\"id\": 1}," +
                "\"tipoContratacao\": \"DIARIA\"," +
                "\"quantidade\": 5," +
                "\"prazo\": 3}," +
                "\"tipoPagamento\": \"DEBITO\"}";


        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/pagamento/editar/{id}", pagamentoEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoPagamento").value("DEBITO")
        );
    }
}

