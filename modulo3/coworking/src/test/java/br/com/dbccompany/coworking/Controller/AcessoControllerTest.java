package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Repository.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AcessoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @BeforeEach
    public void gerarDadosDeEntrada() {
        //Cliente
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newTipoContato);
        contato.setValor("fulano@fulano.com");
        ContatoEntity c1 = contatoRepository.save(contato);

        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("5151515151");
        ContatoEntity c2 = contatoRepository.save(contato2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("novoFulano");
        cliente.setCpf("01987654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);
        ClienteEntity clienteEntity = clienteRepository.save(cliente);
        int idCliente = clienteEntity.getId();

        //Espaco
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 2");
        espaco.setQtdPessoas(30);
        espaco.setValor(300.00);
        EspacoEntity newEspaco = espacoRepository.save(espaco);
        int idEspaco = newEspaco.getId();

        //SaldoCliente
        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(idCliente);
        saldoClienteId.setIdEspaco(idEspaco);

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setVencimento(LocalDate.now().plusDays(30));
        saldoCliente.setQuantidade(10);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        SaldoClienteEntity newSaldoCliente = saldoClienteRepository.save(saldoCliente);
    }

    @AfterEach
    public void limpar() {
        acessoRepository.deleteAll();
        saldoClienteRepository.deleteAll();
        espacoRepository.deleteAll();
        clienteRepository.deleteAll();
        contatoRepository.deleteAll();
        tipoContatoRepository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoConsultadoAcessoEntity () throws  Exception {
        URI uri = new URI("/api/acesso/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmAcessoEntity() throws Exception {
        ClienteEntity clienteEntity = clienteRepository.findByCpf("01987654321");
        int idCliente = clienteEntity.getId();

        EspacoEntity espacoEntity = espacoRepository.findByNome("Area 2");
        int idEspaco = espacoEntity.getId();

        URI uri = new URI("/api/acesso/novo");
        String json = "{\"saldoCliente\": {\"id\": {" +
                "\"idCliente\":" + idCliente + "," +
                "\"idEspaco\":" + idEspaco + "}" +
                "}," +
                "\"excecao\": true}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id")
                .exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.excecao").value(true)
        );
    }

    @Test
    public void deveRetornarUmAcessoEntity() throws Exception {

        ClienteEntity cliente = clienteRepository.findByCpf("01987654321");
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");
        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(cliente.getId());
        saldoClienteId.setIdEspaco(espaco.getId());
        SaldoClienteEntity saldoCliente = saldoClienteRepository.findById(saldoClienteId).get();
        AcessoEntity acesso = new AcessoEntity();
        acesso.setSaldoCliente(saldoCliente);
        acesso.setEntrada(true);
        AcessoEntity acessoEntity = acessoRepository.save(acesso);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/acesso/ver/{id}", acessoEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.excecao")
                .value(false)
        );
    }

    @Test
    public void deveEditarERetornarUmAcesso() throws Exception {

        ClienteEntity cliente = clienteRepository.findByCpf("01987654321");
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");
        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(cliente.getId());
        saldoClienteId.setIdEspaco(espaco.getId());
        SaldoClienteEntity saldoCliente = saldoClienteRepository.findById(saldoClienteId).get();
        AcessoEntity acesso = new AcessoEntity();
        acesso.setSaldoCliente(saldoCliente);
        acesso.setEntrada(true);
        acesso.setExcecao(false);
        AcessoEntity acessoEntity = acessoRepository.save(acesso);



        String json = "{\"saldoCliente\": {\"id\": {" +
                "\"idCliente\":" + cliente.getId() + "," +
                "\"idEspaco\":" + espaco.getId() + "}" +
                "}," +
                "\"excecao\": true}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/acesso/editar/{id}", acessoEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.excecao")
                .value(true)
        );
    }
}

