package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContratacaoDTO> todosContratacao() {
        logger.info("Buscando todas contratacoes.");
        List<ContratacaoDTO> listaDTO = new ArrayList<>();
        for (ContratacaoEntity contratacao : service.todos()) {
            listaDTO.add(new ContratacaoDTO(contratacao));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContratacaoDTO salvar(@RequestBody ContratacaoDTO contratacaoDTO){
        logger.info("Adicionando nova contratacao.");
        return service.salvarContratacaoComQuantidadeTurno(contratacaoDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ContratacaoDTO contratacaoEspecifico(@PathVariable Integer id) {
        logger.info("Buscando contratacao id: " + id);
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO(service.porId(id));
        return contratacaoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContratacaoDTO editarContratacao(@PathVariable Integer id, @RequestBody ContratacaoDTO contratacaoDTO) {
        logger.info("Editando contratacao id: " + id);
        ContratacaoEntity contratacao = contratacaoDTO.convert();
        ContratacaoDTO newDTO = new ContratacaoDTO(service.editar(contratacao, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarContratacao(@PathVariable Integer id) {
        logger.info("Deletando contratacao id: " + id);
        service.deletarPorId(id);
        return "Contratacao removida";
    }
}

