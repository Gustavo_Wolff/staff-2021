package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    List<UsuarioEntity> findAll();
    Optional<UsuarioEntity> findById(Integer id );
    List<UsuarioEntity> findAllById( Integer id);
    UsuarioEntity findByNome(String nome);
    List<UsuarioEntity> findAllByNome(String nome);
    UsuarioEntity findByEmail(String email);
    List<UsuarioEntity> findAllByEmail(String email);
    UsuarioEntity findByLogin(String login);
    UsuarioEntity findByLoginAndSenha(String login, String senha);


}

