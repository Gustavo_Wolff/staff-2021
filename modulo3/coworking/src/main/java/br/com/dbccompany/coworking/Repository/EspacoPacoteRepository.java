package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    List<EspacoPacoteEntity> findAll();
    Optional<EspacoPacoteEntity> findById(Integer id );
    List<EspacoPacoteEntity> findAllById( Integer id);
    List<EspacoPacoteEntity> findByEspaco(EspacoEntity espacoEntity);
    List<EspacoPacoteEntity> findByPacote(PacoteEntity pacote);
    List<EspacoPacoteEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacaoEnum);
    List<EspacoPacoteEntity> findByQuantidade(int quantidade);
    List<EspacoPacoteEntity> findByPrazo(int prazo);

}

