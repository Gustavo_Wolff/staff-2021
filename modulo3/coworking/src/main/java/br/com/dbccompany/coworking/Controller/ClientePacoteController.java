package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import br.com.dbccompany.coworking.Service.ClienteService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/clientePacote")
public class ClientePacoteController {

    @Autowired
    ClientePacoteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientePacoteDTO> todosClientePacote() {
        logger.info("Buscando todos clientePacotes.");
        List<ClientePacoteDTO> listaDTO = new ArrayList<>();
        for (ClientePacoteEntity cliente : service.todos()) {
            listaDTO.add(new ClientePacoteDTO(cliente));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientePacoteDTO salvar(@RequestBody ClientePacoteDTO clientePacoteDTO){
        logger.info("Adicionando novo clientePacote.");
        ClientePacoteEntity clientePacoteEntity = clientePacoteDTO.convert();
        ClientePacoteDTO newDTO = new ClientePacoteDTO(service.salvar(clientePacoteEntity));
        return newDTO;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClientePacoteDTO clientePacoteEspecifico(@PathVariable Integer id) {
        logger.info("Buscando clientePacote id: " + id);
        ClientePacoteDTO clientePacoteDTO = new ClientePacoteDTO(service.porId(id));
        return clientePacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientePacoteDTO editarPacoteCliente(@PathVariable Integer id, @RequestBody ClientePacoteDTO clientePacoteDTO) {
        logger.info("Editando clientePacote id: " + id);
        ClientePacoteEntity clientePacote = clientePacoteDTO.convert();
        ClientePacoteDTO newDTO = new ClientePacoteDTO(service.editar(clientePacote, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarClientePacote(@PathVariable Integer id) {
        logger.info("Deletando clientePacote id: " + id);
        service.deletarPorId(id);
        return "clientePacote removido";
    }
}

