package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class SaldoClienteService extends ServiceAbstract<SaldoClienteRepository, SaldoClienteEntity, SaldoClienteId> {
    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity editar(SaldoClienteEntity entidade, Integer idCliente, Integer idEspaco) {
        try {
            SaldoClienteId newId = new SaldoClienteId(idCliente, idEspaco);
            entidade.setId(newId);
            return repository.save(entidade);
        } catch (Exception e) {
            logger.error("Erro ao editar saldo cliente: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<SaldoClienteEntity> porId(Integer idCliente, Integer idEspaco) {
        try {
            SaldoClienteId newId = new SaldoClienteId(idCliente, idEspaco);
            Optional<SaldoClienteEntity> optionalEntity = repository.findById(newId);
            return optionalEntity;
        } catch (Exception e) {
            logger.error("Erro ao buscar saldo cliente por id: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletarSaldoPorId(Integer idCliente, Integer idEspaco) {
        try {
            SaldoClienteId newId = new SaldoClienteId(idCliente, idEspaco);
            SaldoClienteEntity entity = repository.findById(newId).get();
            repository.delete(entity);
        } catch (Exception e) {
            logger.error("Não foi possível remover a saldo do cliente: " + idCliente + ". No espaço de id: " + idEspaco + ". Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}

