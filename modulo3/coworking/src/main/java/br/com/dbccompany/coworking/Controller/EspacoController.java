package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Service.EspacoService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {

    @Autowired
    EspacoService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoDTO> todosEspaco() {
        logger.info("Buscando todos espacos.");
        List<EspacoDTO> listaDTO = new ArrayList<>();
        for (EspacoEntity espaco : service.todos()) {
            listaDTO.add(new EspacoDTO(espaco));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoDTO salvar(@RequestBody EspacoDTO espacoDTO){
        logger.info("Adicionando novo espaco.");
        EspacoEntity espacoEntity = espacoDTO.convert();
        EspacoDTO newDTO = new EspacoDTO(service.salvar(espacoEntity));
        return newDTO;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public EspacoDTO espacoEspecifico(@PathVariable Integer id) {
        logger.info("Buscando o espaco id: " + id);
        EspacoDTO espacoDTO = new EspacoDTO(service.porId(id));
        return espacoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoDTO editarEspaco(@PathVariable Integer id, @RequestBody EspacoDTO espacoDTO) {
        logger.info("Editando o espaco id: " + id);
        EspacoEntity espaco = espacoDTO.convert();
        EspacoDTO newDTO = new EspacoDTO(service.editar(espaco, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarEspaco(@PathVariable Integer id) {
        logger.info("Deletando espaco id: " + id);
        service.deletarPorId(id);
        return "Espaço removido";
    }
}

