package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.coworking.Util.ConvertePrazoHorasTurno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EspacoPacoteService extends ServiceAbstract<EspacoPacoteRepository, EspacoPacoteEntity, Integer> {

    @Autowired
    EspacoPacoteRepository espacoPacoteRepository;


    public EspacoPacoteDTO salvarEspacoComQuantidadeTurno(EspacoPacoteDTO espacoPacoteDTO) {
        try {
            int quantidade = espacoPacoteDTO.getQuantidade();
            TipoContratacaoEnum tipoContratacaoEnum = espacoPacoteDTO.getTipoContratacao();
            espacoPacoteDTO.setQuantidade(ConvertePrazoHorasTurno.converteTurnoParaHoras(quantidade, tipoContratacaoEnum));

            EspacoPacoteEntity espacoPacoteEntity = espacoPacoteDTO.convert();
            EspacoPacoteDTO newDto = new EspacoPacoteDTO((super.salvar(espacoPacoteEntity)));
            return newDto;
        } catch (Exception e) {
            logger.error("Erro ao salvar espaço: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}

