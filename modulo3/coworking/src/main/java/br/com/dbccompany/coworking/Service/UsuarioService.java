package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Util.ValidaSenhaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<UsuarioDTO> retornarListaUsuarios() {
        try {
            List<UsuarioDTO> listaDTO = new ArrayList<>();
            for (UsuarioEntity usuario : this.todos()) {
                listaDTO.add(new UsuarioDTO(usuario));
            }
            return listaDTO;
        } catch (Exception e) {
            logger.error("Erro ao retornar a lista de usuários: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public UsuarioDTO salvarComValidacaoECriptografiaDeSenha(UsuarioDTO usuarioDTO) {
        UsuarioDTO newDto = new UsuarioDTO();
        try {
            if(ValidaSenhaUtil.validaSenha(usuarioDTO.getSenha())){
                String valorSenha = usuarioDTO.getSenha();
                usuarioDTO.setSenha(passwordEncoder.encode(valorSenha));
                UsuarioEntity usuario = usuarioDTO.convert();
                newDto = new UsuarioDTO(this.salvar(usuario));
            }
        }catch (Exception e) {
            logger.error("Erro salvar usuário com validação de senha: " + e.getMessage());
            throw new RuntimeException();
        } finally {
            return newDto;
        }
    }

    public boolean login(String login , String senha) {
        try {
            UsuarioEntity usuario = repository.findByLoginAndSenha(login, passwordEncoder.encode(senha));
            return usuario == null ? false : true;
        } catch (Exception e) {
            logger.warn("Verifique seu nome de usuário e senha.");
            logger.error("Erro realizar login: " + e.getMessage());
            throw new RuntimeException();
        }
    }

}

