package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    List<ClienteEntity> findAll();
    Optional<ClienteEntity> findById(Integer id );
    List<ClienteEntity> findAllById( Integer id);
    List<ClienteEntity> findByNome(String nome);
    ClienteEntity findByCpf(String cpf);
    List<ClienteEntity> findByDataNascimento(Date dataNascimento);
}

