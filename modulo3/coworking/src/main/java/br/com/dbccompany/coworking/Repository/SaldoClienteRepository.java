package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
    List<SaldoClienteEntity> findAll();
    Optional<SaldoClienteEntity> findById(Integer id );
    List<SaldoClienteEntity> findAllById( Integer id);
    List<SaldoClienteEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    List<SaldoClienteEntity> findByQuantidade(int quantidade);
    List<SaldoClienteEntity> findByVencimento(LocalDate vencimento);

}

