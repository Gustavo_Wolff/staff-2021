package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer>{
    List<AcessoEntity> findAll();
    Optional<AcessoEntity> findById(Integer id );
    List<AcessoEntity> findAllById(Integer id);
    List<AcessoEntity> findBySaldoCliente(SaldoClienteEntity saldoCliente);
    List<AcessoEntity> findByIsEntradaTrue();
    List<AcessoEntity> findByIsEntradaFalse();
    List<AcessoEntity> findByData(LocalDate data);
    List<AcessoEntity> findByIsExcecaoTrue();
    List<AcessoEntity> findByIsExcecaoFalse();
}

