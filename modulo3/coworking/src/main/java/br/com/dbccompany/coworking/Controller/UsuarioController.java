package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Service.UsuarioService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<UsuarioDTO> todosUsuario() {
        logger.info("Buscando todos os usuarios.");
        return service.retornarListaUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public UsuarioDTO salvar(@RequestBody UsuarioDTO usuarioDTO){
        logger.info("Adicionando novo usuario.");
        UsuarioDTO newDto = service.salvarComValidacaoECriptografiaDeSenha(usuarioDTO);
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public UsuarioDTO usuarioEspecifico(@PathVariable Integer id) {
        logger.info("Buscando o usuario id: " + id);
        UsuarioDTO usuarioDTO = new UsuarioDTO(service.porId(id));
        return usuarioDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public UsuarioDTO editarUsuario(@PathVariable Integer id, @RequestBody UsuarioDTO usuarioDTO) {
        logger.info("Editando o usuario id: " + id);
        UsuarioEntity usuario = usuarioDTO.convert();
        UsuarioDTO newDTO = new UsuarioDTO(service.editar(usuario, id));
        return newDTO;
    }

    @PostMapping(value = "/login")
    @ResponseBody
    public boolean fazerLogin(@RequestBody UsuarioDTO login) {
        logger.info(login.getNome() + "Efetuando Login");
        return service.login(login.getLogin(), login.getSenha());
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarUsuario(@PathVariable Integer id) {
        logger.info("Deletando usuario id: " + id);
        service.deletarPorId(id);
        return "Usuario removido";
    }
}

