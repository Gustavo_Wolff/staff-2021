package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Service.AcessoService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    @Autowired
    AcessoService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<AcessoDTO> todosAcesso() {
        logger.info("Buscando todos acessos.");
        List<AcessoDTO> listaDTO = new ArrayList<>();
        for (AcessoEntity acesso : service.todos()) {
            listaDTO.add(new AcessoDTO(acesso));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public AcessoDTO salvar(@RequestBody AcessoDTO acessoDTO){
        logger.info("Adicionando novo acesso.");
        return service.salvarAcesso(acessoDTO);
    }

    @PostMapping(value = "/entrada")
    @ResponseBody
    public AcessoDTO salvarEntrada(@RequestBody Integer id){
        logger.info("Salvando acesso na entrada");
        return service.salvarEntrada(id);
    }

    @PostMapping(value = "/saida")
    @ResponseBody
    public AcessoDTO salvarSaida(@RequestBody Integer id){
        logger.info("Salvando acesso na saída");
        return service.salvarSaida(id);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public AcessoDTO acessoEspecifico(@PathVariable Integer id) {
        logger.info("Buscando acesso de id: " + id);
        AcessoDTO acessoDTO = new AcessoDTO(service.porId(id));
        return acessoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public AcessoDTO editarAcesso(@PathVariable Integer id, @RequestBody AcessoDTO acessoDTO) {
        logger.info("Editando acesso id: " + id);
        AcessoEntity acesso = acessoDTO.convert();
        AcessoDTO newDTO = new AcessoDTO(service.editar(acesso, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarAcesso(@PathVariable Integer id) {
        logger.info("Deletando acesso id: " + id);
        service.deletarPorId(id);
        return "Acesso removido";
    }
}

