package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Service.ClienteService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {
    @Autowired
    ClienteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClienteDTO> todosCliente() {
        logger.info("Buscando todos usuarios.");
        List<ClienteDTO> listaDTO = new ArrayList<>();
        for (ClienteEntity cliente : service.todos()) {
            listaDTO.add(new ClienteDTO(cliente));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteDTO salvar(@RequestBody ClienteDTO clienteDTO) {
        logger.info("Adicionando novo cliente.");
        ClienteEntity clienteEntity = clienteDTO.convert();
        ClienteDTO newDTO = new ClienteDTO(service.salvar(clienteEntity));
        return newDTO;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClienteDTO clienteEspecifico(@PathVariable Integer id) {
        logger.info("Buscando o cliente id: " + id);
        ClienteDTO clienteDTO = new ClienteDTO(service.porId(id));
        return clienteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClienteDTO editarCliente(@PathVariable Integer id, @RequestBody ClienteDTO clienteDTO) {
        logger.info("Editando o cliente id: " + id);
        ClienteEntity cliente = clienteDTO.convert();
        ClienteDTO newDTO = new ClienteDTO(service.editar(cliente, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarCliente(@PathVariable Integer id) {
        logger.info("Deletando cliente id: " + id);
        service.deletarPorId(id);
        return "Cliente removido";
    }
}

