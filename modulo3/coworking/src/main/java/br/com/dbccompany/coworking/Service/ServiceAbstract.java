package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EntityAbstract;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAbstract<
        R extends CrudRepository<E, T>,
        E extends EntityAbstract, T> {

    @Autowired
    protected R repository;

    protected Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Transactional(rollbackFor = Exception.class)
    public E salvar(E entidade) {
        logger.warn("Se tiver campos faltando não será possível salvar a Entidade");
        try{
            return repository.save(entidade);
        } catch (Exception e) {
            logger.error("Erro ao salvar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public E editar(E entidade, T id) {
        try{
            entidade.setId(id);
            return repository.save(entidade);
        } catch (Exception e) {
            logger.error("Erro ao editar a entidade: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public List<E> todos() {
        try {
            return (List<E>) repository.findAll();
        } catch (Exception e) {
            logger.error("Erro ao listar entidades: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public E porId(T id) {
        try {
            return repository.findById(id).get();
        } catch (Exception e) {
            logger.error("Não foi possível encontrar a entidade de id: " + id +". Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletarPorId(T id) {
        try {
            E entity = repository.findById(id).get();
            repository.delete(entity);
        } catch (Exception e) {
            logger.error("Não foi possível remover a entidade de id: " + id +". Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }

}

