package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService extends ServiceAbstract<ClienteRepository, ClienteEntity, Integer> {

    @Autowired
    ContatoRepository contatoRepository;

    public ClienteDTO salvarComContatos(ClienteDTO cliente) {
        try {
            List<ContatoEntity> contato = new ArrayList<>();
            contato.add(validacaoContato("email"));
            contato.add(validacaoContato("telefone"));
            cliente.setContatos(contato);
            ClienteEntity clienteFinal = cliente.convert();
            ClienteDTO newDTO = new ClienteDTO(repository.save(clienteFinal));
            return newDTO;
        } catch (Exception e) {
            logger.error("Erro ao salvar cliente: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    private ContatoEntity validacaoContato(String nome) {
        ContatoEntity contato = contatoRepository.findByValor(nome);
        if(contato == null) {
            contato = contatoRepository.save(new ContatoEntity());
        }
        return contato;
    }
}

