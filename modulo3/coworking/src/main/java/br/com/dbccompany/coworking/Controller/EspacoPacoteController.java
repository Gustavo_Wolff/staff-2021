package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/espacoPacote")
public class EspacoPacoteController {

    @Autowired
    EspacoPacoteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoPacoteDTO> todosEspacoPacote() {
        logger.info("Buscando Todos espacosPacotes.");
        List<EspacoPacoteDTO> listaDTO = new ArrayList<>();
        for (EspacoPacoteEntity espacoPacote : service.todos()) {
            listaDTO.add(new EspacoPacoteDTO(espacoPacote));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoPacoteDTO salvar(@RequestBody EspacoPacoteDTO espacoPacoteDTO){
        logger.info("Adicionando novo espacoPacote.");
        return service.salvarEspacoComQuantidadeTurno(espacoPacoteDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public EspacoPacoteDTO espacoPacoteEspecifico(@PathVariable Integer id) {
        logger.info("Buscando espacoPacote id: " + id);
        EspacoPacoteDTO espacoPacoteDTO = new EspacoPacoteDTO(service.porId(id));
        return espacoPacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoPacoteDTO editarEspacoPacote(@PathVariable Integer id, @RequestBody EspacoPacoteDTO espacoPacoteDTO) {
        logger.info("Editando espacoPacote id: " + id);
        EspacoPacoteEntity espacoPacote = espacoPacoteDTO.convert();
        EspacoPacoteDTO newDTO = new EspacoPacoteDTO(service.editar(espacoPacote, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarEspacoPacote(@PathVariable Integer id) {
        logger.info("Deletando espacoPacote id: " + id);
        service.deletarPorId(id);
        return "Relação espaço-pacote removida";
    }
}

