package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Util.ConvertePrazoHorasTurno;
import org.springframework.stereotype.Service;

@Service
public class ContratacaoService extends ServiceAbstract<ContratacaoRepository, ContratacaoEntity, Integer> {
    public ContratacaoDTO salvarContratacaoComQuantidadeTurno(ContratacaoDTO contratacaoDTO) {
        try {
            int quantidade = contratacaoDTO.getQuantidade();
            TipoContratacaoEnum tipoContratacaoEnum = contratacaoDTO.getTipoContratacao();
            contratacaoDTO.setQuantidade(ConvertePrazoHorasTurno.converteTurnoParaHoras(quantidade, tipoContratacaoEnum));

            ContratacaoEntity contratacaoEntity = contratacaoDTO.convert();
            ContratacaoDTO newDto = new ContratacaoDTO((super.salvar(contratacaoEntity)));
            return newDto;
        } catch (Exception e) {
            logger.error("Erro ao salvar contratacao: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}

