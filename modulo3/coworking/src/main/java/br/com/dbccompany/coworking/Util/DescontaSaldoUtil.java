package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.DTO.AcessoDTO;

public class DescontaSaldoUtil {
    public static int descontaSaldo(AcessoDTO acessoDTO) {
        int quantidade = acessoDTO.getSaldoCliente().getQuantidade();
        switch (acessoDTO.getSaldoCliente().getTipoContratacao()) {
            case DIARIA:
                acessoDTO.getSaldoCliente().setQuantidade(--quantidade);
                break;
            case TURNO:
            case HORA:
            case MINUTO:
                quantidade -= ConverteMillisUtil.converteTimeMillisParaMinutoOuHora(acessoDTO.getTimeEntrada() - acessoDTO.getTimeSaida(),
                        acessoDTO.getSaldoCliente().getTipoContratacao());
                acessoDTO.getSaldoCliente().setQuantidade(quantidade);
                break;
        }
        return quantidade;
    }
}

