package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Service.PacoteService;
import br.com.dbccompany.coworking.CoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {

    @Autowired
    PacoteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PacoteDTO> todosPacote() {
        logger.info("Buscando todos pacotes.");
        List<PacoteDTO> listaDTO = new ArrayList<>();
        for (PacoteEntity pacote : service.todos()) {
            listaDTO.add(new PacoteDTO(pacote));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public PacoteDTO salvar(@RequestBody PacoteDTO pacoteDTO) {
        logger.info("Adicionando novo pacote.");
        PacoteEntity pacoteEntity = pacoteDTO.convert();
        PacoteDTO newDTO = new PacoteDTO(service.salvar(pacoteEntity));
        return newDTO;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PacoteDTO pacoteEspecifico(@PathVariable Integer id) {
        logger.info("Buscando pacote id: " + id);
        PacoteDTO pacoteDTO = new PacoteDTO(service.porId(id));
        return pacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PacoteDTO editarPacote(@PathVariable Integer id, @RequestBody PacoteDTO pacoteDTO) {
        logger.info("Editando pacote id: " + id);
        PacoteEntity pacote = pacoteDTO.convert();
        PacoteDTO newDTO = new PacoteDTO(service.editar(pacote, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarPacote(@PathVariable Integer id) {
        logger.info("Deletando pacote id: " + id);
        service.deletarPorId(id);
        return "Pacote removido";
    }
}

