package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {
    List<PacoteEntity> findAll();
    Optional<PacoteEntity> findById(Integer id );
    List<PacoteEntity> findAllById( Integer id);
    List<PacoteEntity> findByValor(double valor);
}

