package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    List<ContratacaoEntity> findAll();
    Optional<ContratacaoEntity> findById(Integer id );
    List<ContratacaoEntity> findAllById( Integer id);
    List<ContratacaoEntity> findByEspaco(EspacoEntity espaco);
    List<ContratacaoEntity> findByCliente(ClienteEntity cliente);
    List<ContratacaoEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    List<ContratacaoEntity> findByQuantidade(int quantidade);
    List<ContratacaoEntity> findByDesconto(double desconto);
    List<ContratacaoEntity> findByPrazo(int prazo);
}

