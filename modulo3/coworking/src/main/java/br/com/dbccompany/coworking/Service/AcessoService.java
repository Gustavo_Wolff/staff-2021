package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import br.com.dbccompany.coworking.Util.CalculaValorEspacoQuantidadeUsadaUtil;
import br.com.dbccompany.coworking.Util.DescontaSaldoUtil;
import br.com.dbccompany.coworking.Util.TratarValorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class AcessoService extends ServiceAbstract<AcessoRepository, AcessoEntity, Integer> {

    @Autowired
    SaldoClienteRepository saldoRepository;

    @Autowired
    SaldoClienteService saldoClienteService;

    @Autowired
    ContratacaoService contratacaoService;

    @Autowired
    PagamentoService pagamentoService;

    public AcessoDTO salvarAcesso(AcessoDTO acesso) {
        try {
            if(acesso.isEntrada() || acesso.isExcecao()) {
                return this.salvarEntradaComAcessoDTO(acesso);
            } else {
                return new AcessoDTO(this.salvar(acesso.convert()));
            }
        } catch (Exception e) {
            logger.error("Erro ao cadastrar acesso: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public AcessoDTO salvarEntrada(int id) {
        AcessoDTO acesso = new AcessoDTO(this.porId(id));
        return this.salvarEntradaComAcessoDTO(acesso);
    }

    private AcessoDTO salvarEntradaComAcessoDTO (AcessoDTO acesso) {
        try {
            if(acesso.isExcecao()){
                acesso.getSaldoCliente().setQuantidade(999);
                acesso.getSaldoCliente().setVencimento(LocalDate.now().plusDays(1));
                acesso.setTimeEntrada(System.currentTimeMillis());
                acesso.setEntrada(true);
            }
            SaldoClienteId saldoClienteId = new SaldoClienteId(acesso.getSaldoCliente().getId().getIdCliente(), acesso.getSaldoCliente().getId().getIdEspaco());
            SaldoClienteEntity saldo = saldoRepository.findById(saldoClienteId).get();
            if(acesso.isEntrada() && !acesso.isExcecao()){
                if(saldo.getVencimento().compareTo(acesso.getData()) < 0 || saldo.getQuantidade() == 0) {
                    saldo.setQuantidade(0);
                    saldoClienteService.editar(saldo, saldo.getId().getIdCliente(), saldo.getId().getIdEspaco());
                    acesso.setMensagem("Saldo vencido. Por favor, entre em contato com o administrador.");
                }
                acesso.setTimeEntrada(System.currentTimeMillis());
                acesso.setMensagem("Saldo disponivel: " + acesso.getSaldoCliente().getQuantidade());
            } else {
                if(acesso.isExcecao()){
                    acesso.getSaldoCliente().setQuantidade(999);
                    acesso.getSaldoCliente().setVencimento(LocalDate.now().plusDays(1l));
                }
            }
            AcessoEntity acessoEntity = acesso.convert();
            AcessoDTO newDto = new AcessoDTO((super.editar(acessoEntity, acesso.getId())));
            return newDto;
        } catch (Exception e) {
            logger.error("Erro ao realizar a entrada do acesso: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public AcessoDTO salvarSaida(int id) {
        try {
            AcessoDTO acesso = new AcessoDTO(this.porId(id));
            acesso.setEntrada(false);
            acesso.setTimeSaida(System.currentTimeMillis());
            SaldoClienteId idSaldo = new SaldoClienteId(acesso.getSaldoCliente().getId().getIdCliente(), acesso.getSaldoCliente().getId().getIdEspaco());
            SaldoClienteEntity saldo = saldoRepository.findById(idSaldo).get();
            int saldoDescontado = DescontaSaldoUtil.descontaSaldo(acesso);
            int saldoParaReduzir = acesso.getSaldoCliente().getQuantidade();
            saldo.setQuantidade(saldoParaReduzir - saldoDescontado);
            if (acesso.isExcecao()) {
                acesso.setMensagem("Valor total:  " + TratarValorUtil.trataValorSaida(new CalculaValorEspacoQuantidadeUsadaUtil()
                        .calculaValorTotal(saldo.getTipoContratacao(), saldoDescontado, saldo.getEspaco())) );
                saldo.setQuantidade(0);
                saldo.setVencimento(LocalDate.now());
                ContratacaoDTO contratacaoDTO = new ContratacaoDTO();
                contratacaoDTO.setCliente(acesso.getSaldoCliente().getCliente());
                contratacaoDTO.setEspaco(acesso.getSaldoCliente().getEspaco());
                contratacaoDTO.setTipoContratacao(acesso.getSaldoCliente().getTipoContratacao());
                contratacaoDTO.setQuantidade(acesso.getSaldoCliente().getQuantidade());
                contratacaoDTO.setPrazo(1);
                contratacaoService.salvarContratacaoComQuantidadeTurno(contratacaoDTO);
                PagamentoDTO pagamentoDTO = new PagamentoDTO();
                pagamentoDTO.setContratacao(contratacaoDTO.convert());
                pagamentoDTO.setTipoPagamento(TipoPagamentoEnum.DEBITO);
                pagamentoService.salvarAceitandoClientePacoteOuContratacaoOpcional(pagamentoDTO);
            }
            saldoClienteService.editar(saldo, saldo.getId().getIdCliente(), saldo.getId().getIdEspaco());
            AcessoEntity acessoEntity = acesso.convert();
            AcessoDTO newDto = new AcessoDTO((super.editar(acessoEntity, id)));
            return newDto;
        } catch (Exception e) {
            logger.error("Erro ao calcular a saida do acesso: " + e.getMessage());
            throw new RuntimeException();
        }
    }

}

