package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import br.com.dbccompany.lotr.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Repository
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException{
    UsuarioEntity usuario = repository.findByLogin(login);

    if (usuario == null){
        throw new UsernameNotFoundException(("Usuario não encontrado!"));
    }
    /*public UsuarioEntity buscarUsuarioPorLogin (String login) {
        return new UsuarioEntity(repository.findByLogin(login));*/
    return usuario;
    }
}
