package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Service.ItemService;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping( value = "/api/item" )
public class ItemController {

    @Autowired
    private ItemService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ItemDTO salvarItem (@RequestBody ItemDTO item){
        return service.salvar( item.converter() );
    }


    @PutMapping( value = "/editar/{ id }" )
    @ResponseBody
    public ItemDTO editarItem( @RequestBody ItemDTO item, @PathVariable Integer id ){
        return service.editar(item.converter(), id);
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ItemDTO> buscarTodosItens (){
        return service.buscarTodosItens();
    }

    @GetMapping (value = "/buscarId/{id}")
    @ResponseBody
    public ItemDTO buscarItemPorId(@PathVariable Integer id){
        return service.buscarPorId(id);
    }

    @GetMapping(value = "/buscarDescricao/{descricao}")
    @ResponseBody
    public ItemDTO buscarItemPorDescricao(@PathVariable String descricao){
        return service.buscarPorDescricao(descricao);
    }

    @GetMapping(value = "/buscarTodos/{descricao}")
    @ResponseBody
    public List<ItemDTO> buscarTodosItensPorDescricao(@PathVariable String descricao){
        return service.buscarTodosPorDescricao(descricao);
    }

    @PostMapping(value = "/buscarIds")
    @ResponseBody
    public List<ItemDTO> buscarItensPorId(@RequestBody List<Integer> ids){
        return service.buscarPorIds(ids);
    }

    @PostMapping(value = "/buscarInventarioItem")
    @ResponseBody
    public ItemDTO buscarItemPorInventarioItem(@RequestBody List<Inventario_X_ItemDTO> inventariosItens){
        List<Inventario_X_ItemEntity> listaEntity = new ArrayList<>();
        for (Inventario_X_ItemDTO inventarioItem : inventariosItens){
            listaEntity.add(inventarioItem.converter());
        }
        return service.buscarPorInventarioItem(listaEntity);
    }


}

