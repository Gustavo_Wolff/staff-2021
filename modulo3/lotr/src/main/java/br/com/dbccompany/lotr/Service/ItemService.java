package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ItemDTO salvar(ItemEntity item ){
        ItemEntity itemNovo = this.salvarEEditar(item);
        return new ItemDTO(itemNovo);
    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO editar( ItemEntity item, Integer id){
        item.setId(id);
        ItemEntity itemEditado = this.salvarEEditar(item);
        return new ItemDTO(itemEditado);
    }

    public ItemEntity salvarEEditar(ItemEntity item){
        return repository.save(item);
    }

    private List<ItemDTO> listaEntityParaDBO (List<ItemEntity> listaEntity){
        List<ItemDTO> itensDTO = new ArrayList<>();
        for (ItemEntity item : listaEntity){
            itensDTO.add(new ItemDTO(item));
        }
        return itensDTO;
    }

    public List<ItemDTO> buscarTodosItens(){
        return listaEntityParaDBO(repository.findAll());
    }

    public ItemDTO buscarPorId(Integer id){
        Optional<ItemEntity> item = repository.findById(id);
        if(item.isPresent()) {
            return new ItemDTO(repository.findById(id).get());
        }
        return null;
    }

    public List<ItemDTO> buscarPorIds(List<Integer> ids){
        return listaEntityParaDBO(repository.findByIdIn(ids));
    }

    public ItemDTO buscarPorDescricao(String descricao){
        return new ItemDTO(repository.findByDescricao(descricao));
    }

    public List<ItemDTO> buscarTodosPorDescricao(String descricao){
        return listaEntityParaDBO(repository.findAllByDescricao(descricao));
    }

    public ItemDTO buscarPorInventarioItem(List<Inventario_X_ItemEntity> inventarioItem){
        return new ItemDTO(repository.findByInventarioItemIn(inventarioItem));
    }

}

