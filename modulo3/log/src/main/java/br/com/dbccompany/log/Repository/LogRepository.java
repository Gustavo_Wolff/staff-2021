package br.com.dbccompany.log.Repository;

import br.com.dbccompany.log.Entity.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends MongoRepository <LogEntity, String> {

    List<LogEntity> findAll();
    List<LogEntity> findAllById(String id);
    List<LogEntity> findAllByCodigo(String codigo);
    List<LogEntity> findAllByMensagem(String mensagem);
    List<LogEntity> findAllByCodigoAndAndMensagem(String codigo, String mensagem);


}
