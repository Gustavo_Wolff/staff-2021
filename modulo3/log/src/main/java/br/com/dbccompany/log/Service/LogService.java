package br.com.dbccompany.log.Service;

import br.com.dbccompany.log.DTO.LogDTO;
import br.com.dbccompany.log.Entity.LogEntity;
import br.com.dbccompany.log.LogApplication;
import br.com.dbccompany.log.Repository.LogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class LogService {

    @Autowired
    LogRepository repository;

    private Logger logger = logger = LoggerFactory.getLogger(LogApplication.class);

    @Transactional
    public LogDTO salvarLog(LogDTO logsDTO) {
        try {
            LogEntity logFinal = logsDTO.convert();
            LogDTO newDTO = new LogDTO(repository.save(logFinal));
            return newDTO;
        } catch (Exception e) {
            logger.error("Erro ao salvar log: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional
    public List<LogDTO> todos() {
        try {
            List<LogDTO> logsDTOS = new ArrayList<>();
            for (LogEntity logEntity: repository.findAll()) {
                logsDTOS.add(new LogDTO(logEntity));
            }
            return logsDTOS;
        } catch (Exception e) {
            logger.error("Erro ao listar logs: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    public LogDTO porId(String id) {
        try {
            return new LogDTO(repository.findById(id).get());
        } catch (Exception e) {
            logger.error("Nao foi encontrado o log id: " + id +". Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
