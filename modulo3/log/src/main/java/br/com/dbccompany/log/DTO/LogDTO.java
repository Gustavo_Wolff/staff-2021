package br.com.dbccompany.log.DTO;

import br.com.dbccompany.log.Entity.LogEntity;

public class LogDTO {

    private String id;
    private String codigo;
    private String mensagem;

    public LogDTO(){}

    public LogDTO(LogEntity logEntity) {

        this.id = logEntity.getId();
        this.codigo = logEntity.getCodigo();
        this.mensagem = logEntity.getMensagem();

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String toString(){
        return String.format(
                "Log[id=%s, cogigo='%s', mensagem='%s']",
                id, codigo, mensagem );
    }

    public LogEntity convert() {
        LogEntity logEntity = new LogEntity();
        logEntity.setId(this.id);
        logEntity.setCodigo(this.codigo);
        logEntity.setMensagem(this.mensagem);
        return logEntity;
    }

}
