package br.com.dbccompany.log.Controller;

import br.com.dbccompany.log.DTO.LogDTO;
import br.com.dbccompany.log.LogApplication;
import br.com.dbccompany.log.Service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/apiLog")
public class LogController {

    @Autowired
    LogService service;

    private Logger logger = LoggerFactory.getLogger(LogApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<LogDTO> todosLogs() {
        return service.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public LogDTO salvar(@RequestBody LogDTO logDTO){
        return service.salvarLog(logDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public LogDTO espacoEspecifico(@PathVariable String id) {
        return service.porId(id);
    }

}

