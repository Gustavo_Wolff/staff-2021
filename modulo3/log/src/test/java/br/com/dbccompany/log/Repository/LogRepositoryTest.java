package br.com.dbccompany.log.Repository;

import br.com.dbccompany.log.Entity.LogEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataMongoTest
public class LogRepositoryTest {

    @Autowired
    private LogRepository logRepository;

    @AfterEach
    public void limpar() {
        logRepository.deleteAll();
    }

    @Test
    public void salvaLogPorIdECodigoEMensagem() {
        LogEntity logsEntity = new LogEntity();
        logsEntity.setId("aaa111");
        logsEntity.setCodigo("3040aa");
        logsEntity.setMensagem("Nova Mensagem");
        logRepository.save(logsEntity);

        assertEquals("aaa111", logRepository.findAllByMensagem("Nova Mensagem").get(0).getId());
        assertEquals("aaa111", logRepository.findAllByMensagem("Nova Mensagem").get(0).getId());
        assertEquals("aaa111", logRepository.findAllByCodigo("3040aa").get(0).getId());
        assertEquals("aaa111", logRepository.findAllByCodigoAndAndMensagem("3040aa","Nova Mensagem").get(0).getId());
        assertEquals("3040aa", logRepository.findById("aaa111").get().getCodigo());
    }

    @Test
    public void retornaOptionalEmptySeLogForCriado() {

        assertEquals(Optional.empty(),logRepository.findById("aaa111"));
    }

    @Test
    public void salvaDoisLogsERetornaTodos() {
        LogEntity logsEntity = new LogEntity();
        logsEntity.setId("aaa111");
        logsEntity.setCodigo("3040aa");
        logsEntity.setMensagem("Nova Mensagem");
        logRepository.save(logsEntity);

        LogEntity logsEntity2 = new LogEntity();
        logsEntity2.setId("222aaa111");
        logsEntity2.setCodigo("2223040aa");
        logsEntity2.setMensagem("Nova Mensagem 2- O contra-ataque da mensagem");
        logRepository.save(logsEntity2);

        List<LogEntity> logsEntities = logRepository.findAll();

        assertEquals("aaa111", logsEntities.get(0).getId());
        assertEquals("222aaa111", logsEntities.get(1).getId());
    }

}
