package br.com.dbccompany.log.Controller;

import br.com.dbccompany.log.Entity.LogEntity;
import br.com.dbccompany.log.Repository.LogRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
public class LogControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LogRepository repository;

    @AfterEach
    public void limpar() {
        repository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoConsultadoLogs () throws  Exception {
        URI uri = new URI("/apiLog/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmLogsEntity() throws Exception {
        URI uri = new URI("/apiLog/novo");
        String json = "{\"id\": \"5484srs154fs4\"," +
                "    \"codigo\": \"01lff5\"," +
                "    \"mensagem\": \"foi\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo").value("01lff5")
        );
    }

    @Test
    public void deveRetornarUmaEntidadeLog() throws Exception {
        LogEntity logEntity = new LogEntity();
        logEntity.setId("bbbqq");
        logEntity.setCodigo("65po");
        logEntity.setMensagem("Nova Mensagem");
        LogEntity logEntity1 = repository.save(logEntity);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/apiLog/ver/{id}", logEntity1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo")
                .value("65po")
        );
    }

}
